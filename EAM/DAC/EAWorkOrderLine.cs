using System;
using PX.Data;
using PX.Objects.Common;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Objects.CR;
using PX.Objects.TX;
using PX.Objects.GL;
using PX.Objects.CM;
using PX.Objects.IN;
using PX.Objects.CS;
using PX.Objects.AR;
using PX.Objects.SO;
using PX.Objects.PO;

namespace EAM
{
  [Serializable]
  public class EAWorkOrderLine : IBqlTable
  {
    #region EAWOLineNbr
    public abstract class eAWOLineNbr : IBqlField { }
    [PXDBInt(IsKey = true)]
    [PXLineNbr(typeof(EAWorkOrder.wOLineCntr))]
    public virtual Int32? EAWOLineNbr { get; set; }
    #endregion

    #region EAWorkOrderID
    public abstract class eAWorkOrderID : IBqlField { }
    protected String _EAWorkOrderID;
    [PXDBString(10, IsKey = true, IsUnicode = true, InputMask = "")]
    [PXDBDefault(typeof(EAWorkOrder.eAWorkOrderID))]
    [PXParent(typeof(Select<EAWorkOrder, Where<EAWorkOrder.eAWorkOrderID, Equal<Current<EAWorkOrderLine.eAWorkOrderID>>>>))]
    [PXUIField(DisplayName = "Work Order Nbr.", Enabled = false, Visible = false)]
    public virtual string EAWorkOrderID 
    {
        get { return this._EAWorkOrderID;}
        set { this._EAWorkOrderID = value;} 
    }
    #endregion

    #region LineType
    public abstract class lineType : PX.Data.IBqlField
    {
    }
    protected String _LineType;
    [PXDBString(2, IsFixed = true)]
    [PXDefault()]
    [POLineTypeList(
        typeof(EAWorkOrderLine.inventoryID),
        new string[] { POLineType.GoodsForInventory, POLineType.NonStock, POLineType.Service },
        new string[] { PX.Objects.PO.Messages.GoodsForInventory, PX.Objects.PO.Messages.NonStockItem, PX.Objects.PO.Messages.Service })]
    [PXUIField(DisplayName = "Line Type")]
    public virtual String LineType
    {
        get
        {
            return this._LineType;
        }
        set
        {
            this._LineType = value;
        }
    }
    #endregion

    #region LocationID
    public abstract class locationID : PX.Data.IBqlField
    {
    }
    protected Int32? _LocationID;
    [PX.Objects.IN.Location(typeof(EAWorkOrderLine.siteID), DisplayName = "Location", KeepEntry = false, ResetEntry = false, DescriptionField = typeof(INLocation.descr))]
    public virtual Int32? LocationID
    {
        get
        {
            return this._LocationID;
        }
        set
        {
            this._LocationID = value;
        }
    }
    #endregion

    #region CuryInfoID
    public abstract class curyInfoID : PX.Data.IBqlField
    {
    }
    protected Int64? _CuryInfoID;
    [PXDBLong()]
    [CurrencyInfo(typeof(EAWorkOrder.curyInfoID))]
    public virtual Int64? CuryInfoID
    {
        get
        {
            return this._CuryInfoID;
        }
        set
        {
            this._CuryInfoID = value;
        }
    }
    #endregion

    #region Quantity
    public abstract class quantity : IBqlField { }
    [PXDefault(TypeCode.Decimal, "0.0")]
    [PXDBQuantity]
    [PXUIField(DisplayName = "Quantity", Visibility = PXUIVisibility.Visible)]
    public virtual Decimal? Quantity { get; set; }
    #endregion

    #region WOLineDescription
    public abstract class wOLineDescription : IBqlField { }

    [PXDBLocalizableString(60, IsUnicode = true)]
    [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.Visible)]
    public virtual String WOLineDescription { get; set; }
    #endregion

    #region InventoryID
    public abstract class inventoryID : IBqlField { }

    [Inventory(Filterable = true)]
    [PXForeignReference(typeof(Field<inventoryID>.IsRelatedTo<InventoryItem.inventoryID>))]
    public virtual Int32? InventoryID { get; set; }
    #endregion

    #region SubItemID
    public abstract class subItemID : IBqlField { }

    [SubItem(typeof(EAWorkOrderLine.inventoryID))]
    public virtual Int32? SubItemID { get; set; }
    #endregion

    #region UOM
    public abstract class uOM : IBqlField { }

    [PXDefault(typeof(Search<InventoryItem.salesUnit, Where<InventoryItem.inventoryID, Equal<Current<EAWorkOrderLine.inventoryID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    [INUnit(typeof(EAWorkOrderLine.inventoryID), DisplayName = "UOM")]
    public virtual String UOM { get; set; }
    #endregion

    #region CuryExtPrice
    public abstract class curyExtPrice : IBqlField { }

    [PXDBCurrency(typeof(EAWorkOrderLine.curyInfoID), typeof(EAWorkOrderLine.extPrice))]
    [PXUIField(DisplayName = "Ext. Price")]
    [PXFormula(typeof(Mult<EAWorkOrderLine.quantity, EAWorkOrderLine.curyUnitPrice>))]
    [PXDefault(TypeCode.Decimal, "0.0")]
    public virtual Decimal? CuryExtPrice { get; set; }
    #endregion

    #region ExtPrice
    public abstract class extPrice : IBqlField { }

    [PXDBDecimal(4)]
    [PXDefault(TypeCode.Decimal, "0.0")]
    public virtual Decimal? ExtPrice { get; set; }
    #endregion

    #region CuryUnitPrice
    public abstract class curyUnitPrice : IBqlField { }

    [PXDefault(TypeCode.Decimal, "0.0")]
    [PXDBCurrency(typeof(CommonSetup.decPlPrcCst), typeof(EAWorkOrderLine.curyInfoID), typeof(EAWorkOrderLine.unitPrice))]
    [PXUIField(DisplayName = "Unit Price", Visibility = PXUIVisibility.SelectorVisible)]
    public virtual Decimal? CuryUnitPrice { get; set; }
    #endregion

    #region UnitPrice
    public abstract class unitPrice : PX.Data.IBqlField
    {
    }
    protected Decimal? _UnitPrice;
    [PXDBPriceCost()]
    [PXDefault(TypeCode.Decimal, "0.0")]
    [PXUIField(DisplayName = "Unit Price", Enabled = false)]
    public virtual Decimal? UnitPrice
    {
        get
        {
            return this._UnitPrice;
        }
        set
        {
            this._UnitPrice = value;
        }
    }
    #endregion

    #region CustomerID
    public abstract class customerID : PX.Data.IBqlField
    {
    }
    protected Int32? _CustomerID;
    [PXDBInt()]
    //[PXDefault(typeof(EAWorkOrder.customerID))]
    public virtual Int32? CustomerID
    {
        get
        {
            return this._CustomerID;
        }
        set
        {
            this._CustomerID = value;
        }
    }
    #endregion

    #region GroupDiscountRate
    public abstract class groupDiscountRate : PX.Data.IBqlField
    {
    }
    protected Decimal? _GroupDiscountRate;
    [PXDBDecimal(6)]
    [PXDefault(TypeCode.Decimal, "1.0")]
    public virtual Decimal? GroupDiscountRate
    {
        get
        {
            return this._GroupDiscountRate;
        }
        set
        {
            this._GroupDiscountRate = value;
        }
    }
    #endregion

    #region DocumentDiscountRate
    public abstract class documentDiscountRate : PX.Data.IBqlField
    {
    }
    protected Decimal? _DocumentDiscountRate;
    [PXDBDecimal(6)]
    [PXDefault(TypeCode.Decimal, "1.0")]
    public virtual Decimal? DocumentDiscountRate
    {
        get
        {
            return this._DocumentDiscountRate;
        }
        set
        {
            this._DocumentDiscountRate = value;
        }
    }
    #endregion

    #region DiscountID
    public abstract class discountID : PX.Data.IBqlField
    {
    }
    protected String _DiscountID;
    [PXDBString(10, IsUnicode = true)]
    [PXSelector(typeof(Search<ARDiscount.discountID, Where<ARDiscount.type, Equal<DiscountType.LineDiscount>>>))]
    [PXUIField(DisplayName = "Discount Code", Visible = true, Enabled = true)]
    public virtual String DiscountID
    {
        get
        {
            return this._DiscountID;
        }
        set
        {
            this._DiscountID = value;
        }
    }
    #endregion

    #region DiscountSequenceID
    public abstract class discountSequenceID : PX.Data.IBqlField
    {
    }
    protected String _DiscountSequenceID;
    [PXDBString(10, IsUnicode = true)]
    [PXUIField(DisplayName = "Discount Sequence", Visible = false, Enabled = false)]
    public virtual String DiscountSequenceID
    {
        get
        {
            return this._DiscountSequenceID;
        }
        set
        {
            this._DiscountSequenceID = value;
        }
    }
    #endregion

    #region DiscPct
    public abstract class discPct : IBqlField { }

    [PXDBDecimal(6, MinValue = -100, MaxValue = 100)]
    [PXUIField(DisplayName = "Discount, %")]
    [PXDefault(TypeCode.Decimal, "0.0")]
    public virtual Decimal? DiscPct { get; set; }
    #endregion

    #region CuryDiscAmt
    public abstract class curyDiscAmt : IBqlField { }

    [PXDBCurrency(typeof(CommonSetup.decPlPrcCst), typeof(EAWorkOrderLine.curyInfoID), typeof(EAWorkOrderLine.discAmt), MinValue = 0)]
    [PXUIField(DisplayName = "Discount Amount")]
    [PXDefault(TypeCode.Decimal, "0.0")]
    public virtual Decimal? CuryDiscAmt { get; set; }
    #endregion

    #region DiscAmt
    public abstract class discAmt : IBqlField { }

    [PXDBDecimal(4)]
    [PXDefault(TypeCode.Decimal, "0.0")]
    public virtual Decimal? DiscAmt { get; set; }
    #endregion

    #region ManualPrice
    public abstract class manualPrice : PX.Data.IBqlField
    {
    }
    protected Boolean? _ManualPrice;
    [PXDBBool()]
    [PXDefault(false)]
    [PXUIField(DisplayName = "Manual Price")]
    public virtual Boolean? ManualPrice
    {
        get
        {
            return this._ManualPrice;
        }
        set
        {
            this._ManualPrice = value;
        }
    }
    #endregion

    //#region ManualDisc
    //public abstract class manualDisc : IBqlField { }

    //[SOManualDiscMode(typeof(EAWorkOrderLine.curyDiscAmt), typeof(EAWorkOrderLine.discPct))]
    //[PXDBBool]
    //[PXDefault(false)]
    //[PXUIField(DisplayName = "Manual Discount", Visibility = PXUIVisibility.Visible)]
    //public virtual Boolean? ManualDisc { get; set; }
    //#endregion

    #region CSOrderNbr
    public abstract class cSOrderNbr : PX.Data.IBqlField
    {
    }
    protected String _CSOrderNbr;
    [PXDBString(15, IsUnicode = true, InputMask = "")]
    public virtual String CSOrderNbr
    {
        get
        {
            return this._CSOrderNbr;
        }
        set
        {
            this._CSOrderNbr = value;
        }
    }
    #endregion
    #region CSLineNbr
    public abstract class cSLineNbr : PX.Data.IBqlField
    {
    }
    protected Int32? _CSLineNbr;
    [PXDBInt]
    public virtual Int32? CSLineNbr
    {
        get
        {
            return this._CSLineNbr;
        }
        set
        {
            this._CSLineNbr = value;
        }
    }
    #endregion

    #region IsFree
    public abstract class isFree : IBqlField { }

    [PXDBBool]
    [PXDefault(false)]
    [PXUIField(DisplayName = "Free Item")]
    public virtual Boolean? IsFree { get; set; }
    #endregion

    #region Siteid
    public abstract class siteID : PX.Data.IBqlField
    {
    }
    protected Int32? _SiteID;
    [PXFormula(typeof(Default<EAWorkOrderLine.inventoryID>))]
    [POSiteAvail(typeof(EAWorkOrderLine.inventoryID), typeof(EAWorkOrderLine.subItemID))]
    [PXForeignReference(typeof(Field<siteID>.IsRelatedTo<INSite.siteID>))]
    [PXDefault(
        typeof(
        Coalesce<
        Search<InventoryItem.dfltSiteID,
        Where<InventoryItem.inventoryID, Equal<Current<EAWorkOrderLine.inventoryID>>>>,
        Search<EAWorkOrderLine.siteID,
        Where<EAWorkOrderLine.eAWorkOrderID, Equal<Current<EAWorkOrderLine.eAWorkOrderID>>,
        And<EAWorkOrderLine.eAWOLineNbr, Equal<Current<EAWorkOrderLine.eAWOLineNbr>>>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    public virtual Int32? SiteID
    {
        get
        {
            return this._SiteID;
        }
        set
        {
            this._SiteID = value;
        }
    }
    #endregion

    #region Tstamp
    public abstract class Tstamp : PX.Data.IBqlField
    {
    }
    protected Byte[] _tstamp;
    [PXDBTimestamp()]
    public virtual Byte[] tstamp
    {
        get
        {
            return this._tstamp;
        }
        set
        {
            this._tstamp = value;
        }
    }
    #endregion

    #region CreatedByID

    public abstract class createdByID : PX.Data.IBqlField
    {
    }
    protected Guid? _CreatedByID;
    [PXDBCreatedByID()]
    public virtual Guid? CreatedByID
    {
        get
        {
            return this._CreatedByID;
        }
        set
        {
            this._CreatedByID = value;
        }
    }
    #endregion

    #region CreatedByScreenID

    public abstract class createdByScreenID : PX.Data.IBqlField
    {
    }
    protected String _CreatedByScreenID;
    [PXDBCreatedByScreenID()]
    public virtual String CreatedByScreenID
    {
        get
        {
            return this._CreatedByScreenID;
        }
        set
        {
            this._CreatedByScreenID = value;
        }
    }
    #endregion

    #region CreatedDateTime
    public abstract class createdDateTime : PX.Data.IBqlField
    {
    }
    protected DateTime? _CreatedDateTime;
    [PXDBCreatedDateTime()]
    public virtual DateTime? CreatedDateTime
    {
        get
        {
            return this._CreatedDateTime;
        }
        set
        {
            this._CreatedDateTime = value;
        }
    }
    #endregion

    #region LastModifiedByID

    public abstract class lastModifiedByID : PX.Data.IBqlField
    {
    }
    protected Guid? _LastModifiedByID;
    [PXDBLastModifiedByID()]
    public virtual Guid? LastModifiedByID
    {
        get
        {
            return this._LastModifiedByID;
        }
        set
        {
            this._LastModifiedByID = value;
        }
    }
    #endregion

    #region LastModifiedByScreenID

    public abstract class lastModifiedByScreenID : PX.Data.IBqlField
    {
    }
    protected String _LastModifiedByScreenID;
    [PXDBLastModifiedByScreenID()]
    public virtual String LastModifiedByScreenID
    {
        get
        {
            return this._LastModifiedByScreenID;
        }
        set
        {
            this._LastModifiedByScreenID = value;
        }
    }
    #endregion

    #region LastModifiedDateTime
    public abstract class lastModifiedDateTime : PX.Data.IBqlField
    {
    }
    protected DateTime? _LastModifiedDateTime;

    [PXDBLastModifiedDateTime()]
    public virtual DateTime? LastModifiedDateTime
    {
        get
        {
            return this._LastModifiedDateTime;
        }
        set
        {
            this._LastModifiedDateTime = value;
        }
    }
    #endregion

    #region Noteid
    public abstract class noteID : PX.Data.IBqlField
    {
    }
    protected Guid? _NoteID;
    [PXNote()]
    public virtual Guid? NoteID
    {
        get
        {
            return this._NoteID;
        }
        set
        {
            this._NoteID = value;
        }
    }
    #endregion
  }
}