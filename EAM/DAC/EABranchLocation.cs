using PX.Data;
using PX.Objects.CR;
using PX.Objects.CR.MassProcess;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN;
using System;

namespace EAM
{
  //[PXCacheName("Branch Location")]
  //[PXPrimaryGraph(typeof (BranchLocationMaint))]
  [Serializable]
  public class EABranchLocation : IBqlTable
  {
    [PXDBIdentity]
    [PXUIField(Enabled = false)]
    public virtual int? BranchLocationID { get; set; }

    [PXDBString(15, InputMask = ">CCCCCCCCCCCCCCC", IsFixed = true, IsKey = true, IsUnicode = true)]
    [PXSelector(typeof (EABranchLocation.branchLocationCD))]
    [PXUIField(DisplayName = "Branch Location ID", Visibility = PXUIVisibility.SelectorVisible)]
    [PXDefault]
    [NormalizeWhiteSpace]
    public virtual string BranchLocationCD { get; set; }

    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Address Line 1")]
    public virtual string AddressLine1 { get; set; }

    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Address Line 2")]
    public virtual string AddressLine2 { get; set; }

    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Address Line 3")]
    public virtual string AddressLine3 { get; set; }

    [PXDBInt]
    [PXDefault(typeof (AccessInfo.branchID))]
    [PXUIField(DisplayName = "Branch")]
    [PXSelector(typeof (PX.Objects.GL.Branch.branchID), DescriptionField = typeof (PX.Objects.GL.Branch.acctName), SubstituteKey = typeof (PX.Objects.GL.Branch.branchCD))]
    public virtual int? BranchID { get; set; }

    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "City")]
    public virtual string City { get; set; }

    [PXDefault(typeof (Search<PX.Objects.GL.Branch.countryID, Where<PX.Objects.GL.Branch.branchID, Equal<Current<AccessInfo.branchID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    [PXDBString(2, IsFixed = true)]
    [PXUIField(DisplayName = "Country")]
    [PXSelector(typeof (Search<Country.countryID>), CacheGlobal = true, DescriptionField = typeof (Country.description))]
    public virtual string CountryID { get; set; }

    [PXDBString(60, IsUnicode = true)]
    [PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
    [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
    public virtual string Descr { get; set; }

    [PXDBEmail]
    [PXUIField(DisplayName = "Email")]
    public virtual string EMail { get; set; }

    [PXDBString(50)]
    [PhoneValidation]
    [PXUIField(DisplayName = "Fax")]
    public virtual string Fax { get; set; }

    [PXDBBool]
    [PXDefault(false)]
    [PXUIField(DisplayName = "Address Validated", Enabled = false)]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.addressLine1>))]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.addressLine2>))]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.postalCode>))]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.countryID>))]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.city>))]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.state>))]
    public virtual bool? IsValidated { get; set; }

    [PXDBString(50)]
    [PXUIField(DisplayName = "Phone 1")]
    [PhoneValidation]
    public virtual string Phone1 { get; set; }

    [PXDBString(50)]
    [PhoneValidation]
    [PXUIField(DisplayName = "Phone 2")]
    public virtual string Phone2 { get; set; }

    [PXDBString(50)]
    [PhoneValidation]
    [PXUIField(DisplayName = "Phone 3")]
    public virtual string Phone3 { get; set; }

    [PXDBString(20)]
    [PXUIField(DisplayName = "Postal Code")]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.countryID>))]
    public virtual string PostalCode { get; set; }

    [PXDBString(255, IsUnicode = true)]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Attention")]
    public virtual string Salutation { get; set; }

    [PXDBString(50, IsUnicode = true)]
    [PX.Objects.CR.State(typeof (EABranchLocation.countryID), DescriptionField = typeof (PX.Objects.CS.State.name))]
    [PXUIField(DisplayName = "State")]
    [PXFormula(typeof (PX.Data.Default<EABranchLocation.countryID>))]
    public virtual string State { get; set; }

    [SubAccount(DisplayName = "General Subaccount")]
    public virtual int? SubID { get; set; }

    [PXDBString(255, IsUnicode = true)]
    [PXMassMergableField]
    [PXUIField(DisplayName = "Web")]
    public virtual string WebSite { get; set; }

    [PXUIField(DisplayName = "NoteID")]
    [PXNote(new System.Type[] {})]
    public virtual Guid? NoteID { get; set; }

    [PXDBCreatedByID]
    public virtual Guid? CreatedByID { get; set; }

    [PXDBCreatedByScreenID]
    public virtual string CreatedByScreenID { get; set; }

    [PXDBCreatedDateTime]
    public virtual DateTime? CreatedDateTime { get; set; }

    [PXDBLastModifiedByID]
    public virtual Guid? LastModifiedByID { get; set; }

    [PXDBLastModifiedByScreenID]
    public virtual string LastModifiedByScreenID { get; set; }

    [PXDBLastModifiedDateTime]
    public virtual DateTime? LastModifiedDateTime { get; set; }

    [PXDBTimestamp]
    public virtual byte[] tstamp { get; set; }

    [PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
    [Site(DescriptionField = typeof (INSite.descr), DisplayName = "Default Warehouse")]
    public virtual int? DfltSiteID { get; set; }

    [SubItem(DisplayName = "Default Subitem")]
    public virtual int? DfltSubItemID { get; set; }

    [INUnit(DisplayName = "Default Unit")]
    public virtual string DfltUOM { get; set; }

    //[PXBool]
    //[PXFormula(typeof (Current<FSSetup.manageRooms>))]
    //[PXUIField(Visible = false)]
    //public virtual bool? RoomFeatureEnabled { get; set; }

    public abstract class branchLocationID : IBqlField, IBqlOperand
    {
    }

    public abstract class branchLocationCD : IBqlField, IBqlOperand
    {
    }

    public abstract class addressLine1 : IBqlField, IBqlOperand
    {
    }

    public abstract class addressLine2 : IBqlField, IBqlOperand
    {
    }

    public abstract class addressLine3 : IBqlField, IBqlOperand
    {
    }

    public abstract class branchID : IBqlField, IBqlOperand
    {
    }

    public abstract class city : IBqlField, IBqlOperand
    {
    }

    public abstract class countryID : IBqlField, IBqlOperand
    {
    }

    public abstract class descr : IBqlField, IBqlOperand
    {
    }

    public abstract class eMail : IBqlField, IBqlOperand
    {
    }

    public abstract class fax : IBqlField, IBqlOperand
    {
    }

    public abstract class isValidated : IBqlField, IBqlOperand
    {
    }

    public abstract class phone1 : IBqlField, IBqlOperand
    {
    }

    public abstract class phone2 : IBqlField, IBqlOperand
    {
    }

    public abstract class phone3 : IBqlField, IBqlOperand
    {
    }

    public abstract class postalCode : IBqlField, IBqlOperand
    {
    }

    public abstract class salutation : IBqlField, IBqlOperand
    {
    }

    public abstract class state : IBqlField, IBqlOperand
    {
    }

    public abstract class subID : IBqlField, IBqlOperand
    {
    }

    public abstract class webSite : IBqlField, IBqlOperand
    {
    }

    public abstract class noteID : IBqlField, IBqlOperand
    {
    }

    public abstract class createdByID : IBqlField, IBqlOperand
    {
    }

    public abstract class createdByScreenID : IBqlField, IBqlOperand
    {
    }

    public abstract class createdDateTime : IBqlField, IBqlOperand
    {
    }

    public abstract class lastModifiedByID : IBqlField, IBqlOperand
    {
    }

    public abstract class lastModifiedByScreenID : IBqlField, IBqlOperand
    {
    }

    public abstract class lastModifiedDateTime : IBqlField, IBqlOperand
    {
    }

    public abstract class Tstamp : IBqlField, IBqlOperand
    {
    }

    public abstract class dfltSiteID : IBqlField, IBqlOperand
    {
    }

    public abstract class dfltSubItemID : IBqlField, IBqlOperand
    {
    }

    public abstract class dfltUOM : IBqlField, IBqlOperand
    {
    }

    //public abstract class roomFeatureEnabled : IBqlField, IBqlOperand
    //{
    //}
  }
}