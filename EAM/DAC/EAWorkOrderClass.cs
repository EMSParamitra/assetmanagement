using System;
using PX.Data;
using PX.Data.EP;
using PX.Common;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.TX;
using PX.Objects.EP;
using PX.Objects.CM;
using PX.TM;

namespace EAM
{
    [Serializable]
    public class EAWorkOrderClass : IBqlTable
    {
        #region WOClassID
        public abstract class wOClassID : PX.Data.IBqlField { }
        [PXDBIdentity]
        [PXUIField(Visible = false, Visibility = PXUIVisibility.Invisible)]
        public virtual int? WOClassID { get; set; }
        #endregion

        #region WOClassCD
        public abstract class wOClassCD : PX.Data.IBqlField { }
        protected String _WOClassCD;
        [PXDefault()]
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = "")]
        [PXUIField(DisplayName = "WorkOrder Class", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(EAWorkOrderClass.wOClassCD), DescriptionField = typeof(EAWorkOrderClass.descr))]
        public virtual string WOClassCD
        {
            get { return this._WOClassCD; }
            set { this._WOClassCD = value; }
        }
        #endregion

        #region Descr
        public abstract class descr : PX.Data.IBqlField { }
        [PXDBString(60, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
        [PXFieldDescription]
        public virtual string Descr { get; set; }
        #endregion

        #region Active

        public abstract class active : IBqlField { }
        protected Boolean? _Active;
        [PXDBBool]
        [PXDefault(true)]
        [PXUIField(DisplayName = "Active")]
        public virtual Boolean? Active
        {
            get { return this._Active; }
            set { this._Active = value; }
        }
        #endregion

        #region WOTypeID
        public abstract class wOTypeID : PX.Data.IBqlField { }
        [PXDBString(10, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "WorkOrder Type", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<EAWorkOrderType.wOTypeID>), DescriptionField = typeof(EAWorkOrderType.description))]
        public virtual string WOTypeID { get; set; }
        #endregion

        #region WONumberingID
        public abstract class wONumberingID : IBqlField { }
        protected String _WONumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Search<Numbering.numberingID>), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Work Order Numbering Sequence")]
        public virtual string WONumberingID
        {
            get { return this._WONumberingID; }
            set { this._WONumberingID = value; }
        }

        #endregion

        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion

        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion

        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        [PXUIField(DisplayName = "Created Date", Enabled = false, IsReadOnly = true)]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion

        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion

        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion

        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        [PXUIField(DisplayName = "Last Modified Date", Enabled = false, IsReadOnly = true)]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion

        #region Tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion

        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote(DescriptionField = typeof(EAEquipmentClass.eQClassCD),
        Selector = typeof(EAEquipmentClass.eQClassCD))]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
    }
}