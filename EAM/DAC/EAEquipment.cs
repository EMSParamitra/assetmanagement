using System;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.SO;
using PX.Objects.CR;
using PX.Objects.FA;
using PX.Objects.IN;
using PX.Objects.GL;
using PX.Objects.CS;
using PX.Objects.PO;
using PX.SM;
using PX.Data.ReferentialIntegrity.Attributes;

namespace EAM
{
  [Serializable]
  public class EAEquipment : IBqlTable
  {
    #region EAEquipmentID
    public abstract class eAEquipmentID : IBqlField, IBqlOperand { }
    [PXDBIdentity]
    public virtual int? EAEquipmentID { get; set; }
    #endregion

    #region EAEquipmentCD
    public abstract class eAEquipmentCD : PX.Data.IBqlField { }
    protected String _EAEquipmentCD;
    [PXDefault]
    [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
    [PXUIField(DisplayName = "Equipment Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
    [AutoNumber(typeof(EAEquipmentClass.eQNumberingID), typeof(AccessInfo.businessDate))]
    [EASelectorEquipmentNbr]
    public virtual string EAEquipmentCD 
    { 
      get { return this._EAEquipmentCD; } 
      set { this._EAEquipmentCD= value; } 
    }
    #endregion

    #region BranchID
    [PXDBInt()]
    [PXDefault(typeof(AccessInfo.branchID), PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Branch")]
    [PXSelector(typeof (Search<PX.Objects.GL.Branch.branchID>), DescriptionField = typeof (PX.Objects.GL.Branch.acctName), SubstituteKey = typeof (PX.Objects.GL.Branch.branchCD))]
    public virtual int? BranchID { get; set; }
    public abstract class branchID : IBqlField, IBqlOperand { }
    #endregion

    #region BranchLocationID
    [PXDBInt]
    [PXUIField(DisplayName = "Branch Location")]
    [PXSelector(typeof (Search<EABranchLocation.branchLocationID, Where<EABranchLocation.branchID, Equal<Current<EAEquipment.branchID>>>>), DescriptionField = typeof (EABranchLocation.descr), SubstituteKey = typeof (EABranchLocation.branchLocationCD))]
    [PXFormula(typeof (PX.Data.Default<EAEquipment.branchID>))]
    public virtual int? BranchLocationID { get; set; }
    public abstract class branchLocationID : IBqlField, IBqlOperand { }
    #endregion

    #region ColorID
    [PXDBInt]
    [PXUIField(DisplayName = "Color ID")]
    [PXSelector(typeof (SystemColor.colorID), SubstituteKey = typeof (SystemColor.colorName))]
    public virtual int? ColorID { get; set; }
    public abstract class colorID : IBqlField, IBqlOperand { }
    #endregion

    #region LicPlateNbr
    public abstract class licPlateNbr : PX.Data.IBqlField { }
    protected string _LicPlateNbr;
    [PXDBString(10, IsUnicode = true)]
    [PXUIField(DisplayName = "Licensed Plate Nbr.", Visibility = PXUIVisibility.Visible)]
    public virtual string LicPlateNbr
    {
      get { return this._LicPlateNbr; }
      set { this._LicPlateNbr = value; }
    }
    #endregion
      
    #region VinNbr
    public abstract class vinNbr : PX.Data.IBqlField { }
    protected string _VinNbr;
    [PXDBString(20, IsUnicode = true)]
    [PXUIField(DisplayName = "VIN Nbr.", Visibility = PXUIVisibility.Visible)]
    public virtual string VinNbr
    {
      get { return this._VinNbr; }
      set { this._VinNbr = value; }
    }
    #endregion
      
    #region FixedAssetNbr
    public abstract class fixedAssetNbr : PX.Data.IBqlField { }
    protected int? _FixedAssetNbr;
    [PXDBInt]
    [PXUIField(DisplayName = "Fixed Asset Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
    [PXSelector(typeof(Search<FixedAsset.assetID>), SubstituteKey = typeof(FixedAsset.assetCD), DescriptionField = typeof(FixedAsset.description))]
    public virtual int? FixedAssetNbr
    {
      get { return this._FixedAssetNbr; }
      set { this._FixedAssetNbr= value; }
    }
    #endregion
    
    #region CustomerID
    [PXDBInt()]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Customer")]
    [EASelectorCustomer]
    public virtual int? CustomerID { get; set; }
    public abstract class customerID : IBqlField, IBqlOperand { }
    #endregion

    #region CustomerLocationID
    [PX.Objects.CS.LocationID(typeof (Where<Location.bAccountID, Equal<Current<EAEquipment.customerID>>>), DescriptionField = typeof (Location.descr), DirtyRead = true, DisplayName = "Location")]
    [PXDefault(typeof (Search<PX.Objects.CR.BAccount.defLocationID, Where<PX.Objects.CR.BAccount.bAccountID, Equal<Current<EAEquipment.customerID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    [PXFormula(typeof (PX.Data.Default<EAEquipment.customerID>))]
    public virtual int? CustomerLocationID { get; set; }
    public abstract class customerLocationID : IBqlField, IBqlOperand { }
    #endregion

    #region CpnyWarrantyEndDate
    [PXDBDate]
    [PXUIField(DisplayName = "Warranty End Date", Enabled = false)]
    public virtual DateTime? CpnyWarrantyEndDate { get; set; }
    public abstract class cpnyWarrantyEndDate : IBqlField, IBqlOperand { }
    #endregion

    #region CpnyWarrantyType
    [PXDBString(1, IsFixed = true)]
    [ListField_WarrantyType.ListAtrribute]
    [PXDefault("M")]
    [PXUIField]
    public virtual string CpnyWarrantyType { get; set; }
    public abstract class cpnyWarrantyType : ListField_WarrantyType { }
    #endregion

    #region CpnyWarrantyValue
    [PXDBInt(MinValue = 0)]
    [PXUIField(DisplayName = "Company Warranty")]
    public virtual int? CpnyWarrantyValue { get; set; }
    public abstract class cpnyWarrantyValue : IBqlField, IBqlOperand { }
    #endregion

    #region WarrantyStatus
    public abstract class warrantyStatus : IBqlField { }
    protected string _WarrantyStatus;
    [PXDBString(1, IsFixed = true)]
    [ListField_Warranty_Status.ListAtrribute]
    [PXDefault("A")]
    [PXUIField(DisplayName = "Warranty Status")]
    public virtual string WarrantyStatus
    {
        get { return this._WarrantyStatus; }
        set { this._WarrantyStatus = value; }
    }
    #endregion

    #region VendorID
    [PXDBInt]
    [EASelectorBusinessAccount_VE]
    [PXUIField(DisplayName = "Vendor")]
    public virtual int? VendorID { get; set; }
    public abstract class vendorID : IBqlField, IBqlOperand { }
    #endregion

    #region VendorLocationID
    [PX.Objects.CS.LocationID(typeof (Where<Location.bAccountID, Equal<Current<EAEquipment.vendorID>>>), DescriptionField = typeof (Location.descr), DirtyRead = true, DisplayName = "Location")]
    [PXDefault(typeof (Search<PX.Objects.CR.BAccount.defLocationID, Where<PX.Objects.CR.BAccount.bAccountID, Equal<Current<EAEquipment.vendorID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    [PXFormula(typeof (PX.Data.Default<EAEquipment.vendorID>))]
    public virtual int? VendorLocationID { get; set; }
    public abstract class vendorLocationID : IBqlField, IBqlOperand { }
    #endregion

    #region Description
    [PXDBString(255, IsUnicode = true)]
    [PXUIField(DisplayName = "Description")]
    public virtual string Description { get; set; }
    public abstract class description : IBqlField, IBqlOperand { }
    #endregion

    #region DisposalDate
    [PXDBDate]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Disposal Date")]
    public virtual DateTime? DisposalDate { get; set; }
    public abstract class disposalDate : IBqlField, IBqlOperand { }
    #endregion

    #region EngineNo
    [PXDBString(30, IsUnicode = true)]
    [PXUIField(DisplayName = "Engine Nbr")]
    public virtual string EngineNo { get; set; }
    public abstract class engineNo : IBqlField, IBqlOperand { }
    #endregion

    #region EQTypeID
    public abstract class eQTypeID : IBqlField {}
    [PXDBString(15, IsUnicode = true)]
    [PXDefault]
    [PXSelector(typeof(Search<EAEquipmentType.eQTypeID>), DescriptionField = typeof(EAEquipmentType.description))]
    [PXUIField(DisplayName = "Equipment Type", Visibility = PXUIVisibility.SelectorVisible)]
    //[PXDefault(typeof(Search<EAEquipmentClass.eQTypeID, Where<Current<EAEquipment.eQClassID>>)]
    //[PXFormula(typeof(Switch<Case<Where<FixedAsset.classID, IsNotNull>, Selector<FixedAsset.classID, FAClass.assetTypeID>>, Null>))]
    public virtual string EQTypeID { get; set; }
    #endregion

    #region EQClassID
    [PXDBInt]
    [PXUIField(DisplayName = "Equipment Class")]
    [PXDefault()]
    [EASelectorEquipmentClass]
    public virtual int? EQClassID { get; set; }
    public abstract class eQClassID : IBqlField, IBqlOperand { }
    #endregion

    #region EQAccountID
    protected Int32? _EQAccountID;
    [Account(DisplayName = "Equipment Account", Visibility = PXUIVisibility.Visible, DescriptionField = typeof(Account.description))]
    [PXDefault(typeof(Search<EAEquipmentClass.eQAccountID, Where<EAEquipmentClass.eQClassID, Equal<Current<EAEquipment.eQClassID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    public virtual Int32? EQAccountID 
    { 
      get { return this._EQAccountID; } 
      set { this._EQAccountID = value; }
    }
    public abstract class eQAccountID : IBqlField, IBqlOperand { }
    #endregion

    #region EQSubID
    public abstract class eQSubID : IBqlField, IBqlOperand { }
    protected Int32? _EQSubID;
    [SubAccount(typeof(EAEquipment.eQAccountID), DisplayName = "Equipment Sub.", Visibility = PXUIVisibility.Visible, DescriptionField = typeof(Sub.description))]
    [PXDefault(typeof(Search<EAEquipmentClass.eQSubID, Where<EAEquipmentClass.eQClassID, Equal<Current<EAEquipment.eQClassID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    public virtual Int32? EQSubID 
    { 
       get { return this._EQSubID; } 
       set { this._EQSubID = value; } 
    }
    #endregion

    #region EQCostCentre
    public abstract class eQCostCentre : IBqlField, IBqlOperand { } 
    protected Int32? _EQCostCentre;
    [PXDBInt()]
    [PXUIField(DisplayName = "Cost Centre", Enabled = false)]
    //[PXDefault(typeof(EAEquipment.eQSubID, Where<EAEquipment.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>>))]
    [PXDefault(typeof(Search<EAEquipment.eQSubID, Where<EAEquipment.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    public virtual Int32? EQCostCentre 
    { 
      get { return this._EQCostCentre; }
      set { this._EQCostCentre = value; }
    }
    #endregion

    #region INSerialNumber
    public abstract class iNSerialNumber : IBqlField, IBqlOperand { }
    [PXDBString(60, IsUnicode = true)]
    [PXUIField(DisplayName = "Model Serial Number", Enabled = false)]
    public virtual string INSerialNumber { get; set; }
    #endregion

    #region InventoryID
    public abstract class inventoryID : IBqlField { }

    [Inventory(Filterable = true)]
    [PXForeignReference(typeof(Field<inventoryID>.IsRelatedTo<InventoryItem.inventoryID>))]
    public virtual Int32? InventoryID { get; set; }
    #endregion

    #region LocationID
    public abstract class locationID : IBqlField, IBqlOperand { }
    [Location(typeof (EAEquipment.siteID), DescriptionField = typeof (INLocation.descr), DisplayName = "Warehouse Location", Enabled = false, KeepEntry = false)]
    public virtual int? LocationID { get; set; }
    #endregion

    #region LocationType
    [PXDBString(2, IsFixed = true)]
    [PXDefault("CU", PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Location Type")]
    [ListField_LocationType.ListAtrribute]
    public virtual string LocationType { get; set; }
    public abstract class locationType : ListField_LocationType { }
    #endregion

    #region ManufacturerID
    [PXDBInt()]
    [PXUIField(DisplayName = "Manufacturer ID")]
    public virtual int? ManufacturerID { get; set; }
    public abstract class manufacturerID : IBqlField { }
    #endregion

    #region ManufacturerModelID
    [PXDBInt()]
    [PXUIField(DisplayName = "Manufacturer Model ID")]
    public virtual int? ManufacturerModelID { get; set; }
    public abstract class manufacturerModelID : IBqlField { }
    #endregion

    #region ManufacturingYear
    public abstract class manufacturingYear : IBqlField, IBqlOperand { }
    protected string _ManufacturingYear;
    [PXDBString(4, IsFixed = true)]
    [PXUIField(DisplayName = "Model Year")]
    public virtual string ManufacturingYear
    { 
      get { return this._ManufacturingYear; } 
      set { this._ManufacturingYear = value; } 
    }
    #endregion

    #region OwnerID
    [PXDBInt()]
    [PXUIField(DisplayName = "Customer")]
    [EASelectorCustomer]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXFormula(typeof(PX.Data.Default<EAEquipment.ownerType>))]    
    public virtual int? OwnerID { get; set; }
    public abstract class ownerID : IBqlField, IBqlOperand { }
    #endregion

    #region OwnerType
    [PXDBString(2, IsFixed = true)]
    [PXUIField(DisplayName = "Owner Type")]
    [PXDefault("OW")]//, PersistingCheck = PXPersistingCheck.Nothing)]
    [ListField_OwnerType_Equipment.ListAtrribute]
    public virtual string OwnerType { get; set; }
    public abstract class ownerType : ListField_OwnerType_Equipment { }
    #endregion

    #region POOrderType
    public abstract class pOOrderType : PX.Data.IBqlField
    {
    }
    protected String _POOrderType;
    [PXDBString(2, IsFixed = true)]
    [PXDefault(typeof(Search<POOrder.orderType, Where<POOrder.orderNbr, Equal<Current<EAEquipment.pOOrderNbr>>>>), PersistingCheck=PXPersistingCheck.Nothing)]
    public virtual String POOrderType
    {
      get
      {
        return this._POOrderType;
      }
      set
      {
        this._POOrderType = value;
      }
    }
    #endregion

    #region POOrderDate
    protected DateTime? _POOrderDate;
    [PXDBDate()]
    [PXUIField(DisplayName = "Purchase Date", Enabled = false)]
    public virtual DateTime? POOrderDate 
    { 
        get { return this._POOrderDate; } 
        set { this._POOrderDate = value; }
    }
    public abstract class pOOrderDate : IBqlField { }
    #endregion

    #region POOrderNbr
    protected string _POOrderNbr;
    [PXDBString(15, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Purchase Order Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
    [PXSelector(typeof(Search<POOrder.orderNbr>), DescriptionField = typeof(POOrder.orderDesc))]
    public virtual string POOrderNbr 
    { 
      get { return this._POOrderNbr; } 
      set { this._POOrderNbr = value; } 
    }
    public abstract class pOOrderNbr : IBqlField { }
    #endregion

    #region POReceiptNbr
    protected string _POReceiptNbr;
    [PXDBString(15, IsUnicode = true)]
    [PXUIField(DisplayName = "Purchase Receipt Nbr", Enabled = false)]
    public virtual string POReceiptNbr
    { 
      get { return this._POReceiptNbr; } 
      set { this._POReceiptNbr = value; } 
    }
    public abstract class pOReceiptNbr : IBqlField { }
    #endregion

    #region RegisteredDate
    [PXDBDate()]
    [PXUIField(DisplayName = "Registered Date")]
    public virtual DateTime? RegisteredDate { get; set; }
    public abstract class registeredDate : IBqlField { }
    #endregion

    #region RegistrationNbr
    [PXDBString(30, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Registration Nbr")]
    public virtual string RegistrationNbr { get; set; }
    public abstract class registrationNbr : IBqlField { }
    #endregion

    #region ReplaceEquipmentID
    [PXDBInt()]
    [PXUIField(DisplayName = "Replace Equipment ID")]
    public virtual int? ReplaceEquipmentID { get; set; }
    public abstract class replaceEquipmentID : IBqlField { }
    #endregion

    //#region RequireMaintenance
    //[PXDBBool()]
    //[PXUIField(DisplayName = "Require Maintenance")]
    //public virtual bool? RequireMaintenance { get; set; }
    //public abstract class requireMaintenance : IBqlField { }
    //#endregion

    #region SalesDate
    [PXDBDate()]
    [PXUIField(DisplayName = "Delivery Date")]
    public virtual DateTime? SalesDate { get; set; }
    public abstract class salesDate : IBqlField { }
    #endregion

    #region SalesOrderNbr
    protected string _SalesOrderNbr;
    [PXDBString(15, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Sales Order Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
    [PXSelector(typeof(Search<SOOrder.orderNbr>), DescriptionField = typeof(SOOrder.orderDesc))]
    public virtual string SalesOrderNbr 
    {
        get { return this._SalesOrderNbr;}
        set { this._SalesOrderNbr = value;} 
    }
    public abstract class salesOrderNbr : IBqlField { }
    #endregion

    #region SalesOrderType
    protected string _SalesOrderType;
    [PXDBString(3, IsFixed = true, InputMask = "")]
    [PXUIField(DisplayName = "Sales Order Type")]
    [PXDefault(typeof(Search<SOOrder.orderType, Where<SOOrder.orderNbr, Equal<Current<EAEquipment.salesOrderNbr>>>>), PersistingCheck=PXPersistingCheck.Nothing)]
    public virtual string SalesOrderType 
    {
        get { return this._SalesOrderType;}
        set { this._SalesOrderType = value;} 
    }
    public abstract class salesOrderType : IBqlField { }
    #endregion

    #region SerialNumber
    [PXDBString(60, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Serial Number")]
    public virtual string SerialNumber { get; set; }
    public abstract class serialNumber : IBqlField { }
    #endregion

    #region Siteid
    public abstract class siteID : IBqlField, IBqlOperand { }
    [Site(DescriptionField = typeof (INSite.descr), DisplayName = "Warehouse", Enabled = false)]
    public virtual int? SiteID { get; set; }
    #endregion

    #region SourceDocType
    [PXDBString(3, IsFixed = true, InputMask = "")]
    [PXUIField(DisplayName = "Source Doc Type")]
    public virtual string SourceDocType { get; set; }
    public abstract class sourceDocType : IBqlField { }
    #endregion

    #region SourceID
    [PXDBInt()]
    [PXUIField(DisplayName = "Source ID")]
    public virtual int? SourceID { get; set; }
    public abstract class sourceID : IBqlField { }
    #endregion

    #region SourceRefNbr
    [PXDBString(15, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Source Ref Nbr")]
    public virtual string SourceRefNbr { get; set; }
    public abstract class sourceRefNbr : IBqlField { }
    #endregion

    //#region SourceType
    //[PXDBString(3, IsFixed = true, InputMask = "")]
    //[PXUIField(DisplayName = "Source Type")]
   // public virtual string SourceType { get; set; }
   // public abstract class sourceType : IBqlField { }
   // #endregion

    #region Status
    public abstract class status : ListField_Equipment_Status { }
    protected string _Status;
    [PXDBString(1, IsFixed = true)]
    [ListField_Equipment_Status.ListAtrribute]
    [PXDefault("A")]
    [PXUIField(DisplayName = "Status")]
    public virtual string Status
    { 
      get { return this._Status; } 
      set { this._Status = value; } 
    }
    #endregion

    #region SubItemID
    [PXDBInt()]
    [PXUIField(DisplayName = "Sub Item ID")]
    public virtual int? SubItemID { get; set; }
    public abstract class subItemID : IBqlField { }
    #endregion

    #region TagNbr
    [PXDBString(20, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Tag Nbr")]
    public virtual string TagNbr { get; set; }
    public abstract class tagNbr : IBqlField { }
    #endregion

    #region OwnerRevID
    public abstract class ownerRevID : IBqlField
    {
    }
    protected Int32? _OwnerRevID;
    [PXDBInt]
    public virtual Int32? OwnerRevID
    {
        get
        {
            return _OwnerRevID;
        }
        set
        {
            _OwnerRevID = value;
        }
    }
    #endregion

    #region CreatedByID

    [PXDBCreatedByID()]
    public virtual Guid? CreatedByID { get; set; }
    public abstract class createdByID : IBqlField { }
    #endregion

    #region CreatedByScreenID

    [PXDBCreatedByScreenID()]
    public virtual string CreatedByScreenID { get; set; }
    public abstract class createdByScreenID : IBqlField { }
    #endregion

    #region CreatedDateTime
    [PXDBCreatedDateTime]
    public virtual DateTime? CreatedDateTime { get; set; }
    public abstract class createdDateTime : IBqlField { }
    #endregion

    #region LastModifiedByID

    [PXDBLastModifiedByID()]
    public virtual Guid? LastModifiedByID { get; set; }
    public abstract class lastModifiedByID : IBqlField { }
    #endregion

    #region LastModifiedByScreenID

    [PXDBLastModifiedByScreenID()]
    public virtual string LastModifiedByScreenID { get; set; }
    public abstract class lastModifiedByScreenID : IBqlField { }
    #endregion

    #region LastModifiedDateTime
    [PXDBLastModifiedDateTime]
    public virtual DateTime? LastModifiedDateTime { get; set; }
    public abstract class lastModifiedDateTime : IBqlField { }
    #endregion

    #region Noteid
    [PXNote(new System.Type[] {})]
    [PXUIField(DisplayName = "Noteid")]
    public virtual Guid? Noteid { get; set; }
    public abstract class noteid : IBqlField, IBqlOperand { }
    #endregion

    #region Tstamp
    [PXDBTimestamp()]
    [PXUIField(DisplayName = "Tstamp")]
    public virtual byte[] Tstamp { get; set; }
    public abstract class tstamp : IBqlField { }
    #endregion
      
    #region Attributes
    public abstract class attributes : IBqlField { }

    [CRAttributesField(typeof(EAEquipment.eQClassID))]
    public virtual string[] Attributes { get; set; }
    #endregion
  }
}