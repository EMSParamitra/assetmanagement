using System;
using PX.Data;

namespace EAM
{
  [Serializable]
  public class EASetup : IBqlTable
  {
    #region DefaultEQClass
    public abstract class defaultEQClass : PX.Data.IBqlField
    {
    }
    protected String _DefaultEQClass;
    [PXDBString(15, IsFixed = true, InputMask = ">aaaaaaaaaaaaaaa")]
    [PXUIField(DisplayName = "Default Equipment Class")]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXSelector(typeof(Search<EAEquipmentClass.eQClassCD>), DescriptionField = typeof(EAEquipmentClass.descr))]
    public virtual String DefaultEQClass
    {
      get
      {
        return this._DefaultEQClass;
      }
      set
      {
        this._DefaultEQClass= value;
      }
    }
    #endregion

    #region DefaultWOClass
    public abstract class defaultWOClass : PX.Data.IBqlField
    {
    }
    protected String _DefaultWOClass;
    [PXDBString(15, IsFixed = true, InputMask = ">aaaaaaaaaaaaaaa")]
    [PXUIField(DisplayName = "Default WorkOrder Class")]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXSelector(typeof(Search<EAWorkOrderClass.wOClassCD>), DescriptionField = typeof(EAWorkOrderClass.descr))]
    public virtual String DefaultWOClass
    {
        get
        {
            return this._DefaultWOClass;
        }
        set
        {
            this._DefaultWOClass = value;
        }
    }
    #endregion

    #region CreatedByID

    [PXDBCreatedByID()]
    public virtual Guid? CreatedByID { get; set; }
    public abstract class createdByID : IBqlField { }
    #endregion

    #region CreatedByScreenID

    [PXDBCreatedByScreenID()]
    public virtual string CreatedByScreenID { get; set; }
    public abstract class createdByScreenID : IBqlField { }
    #endregion

    #region CreatedDateTime
    [PXDBDate()]
    [PXUIField(DisplayName = "Created Date Time")]
    public virtual DateTime? CreatedDateTime { get; set; }
    public abstract class createdDateTime : IBqlField { }
    #endregion

    #region LastModifiedByID

    [PXDBLastModifiedByID()]
    public virtual Guid? LastModifiedByID { get; set; }
    public abstract class lastModifiedByID : IBqlField { }
    #endregion

    #region LastModifiedByScreenID

    [PXDBLastModifiedByScreenID()]
    public virtual string LastModifiedByScreenID { get; set; }
    public abstract class lastModifiedByScreenID : IBqlField { }
    #endregion

    #region LastModifiedDateTime
    [PXDBDate()]
    [PXUIField(DisplayName = "Last Modified Date Time")]
    public virtual DateTime? LastModifiedDateTime { get; set; }
    public abstract class lastModifiedDateTime : IBqlField { }
    #endregion

    #region Tstamp
    [PXDBTimestamp()]
    [PXUIField(DisplayName = "Tstamp")]
    public virtual byte[] Tstamp { get; set; }
    public abstract class tstamp : IBqlField { }
    #endregion
  }
}