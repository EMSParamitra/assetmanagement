using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PX.Data;

namespace EAM
{
  [System.SerializableAttribute()]  
  public partial class EAWOOrder : PX.Data.IBqlTable
  {    
    #region EAWorkOrderNbr
    public abstract class eAWorkOrderNbr : PX.Data.IBqlField
    {
    }
    protected String _EAWorkOrderNbr;
    [PXDBString(10, IsUnicode = true, IsKey = true, InputMask = "")]
    [PXDefault(typeof(EAWorkOrder.eAWorkOrderID))]
    [PXParent(typeof(Select<EAWorkOrder, Where<EAWorkOrder.eAWorkOrderID, Equal<Current<EAWorkOrder.eAWorkOrderID>>>>))]
    public virtual String EAWorkOrderNbr
    {
      get
      {
        return this._EAWorkOrderNbr;
      }
      set
      {
        this._EAWorkOrderNbr= value;
      }
    }
    #endregion
    #region OrderCategory
    public abstract class orderCategory : PX.Data.IBqlField
    {
    }
    protected String _OrderCategory;
    [PXDBString(2, IsKey = true, IsFixed = true, InputMask = ">aa")]
    [PXDefault()]
    public virtual String OrderCategory
    {
      get
      {
        return this._OrderCategory;
      }
      set
      {
        this._OrderCategory = value;
      }
    }
    #endregion
    #region OrderType
    public abstract class orderType : PX.Data.IBqlField
    {
    }
    protected String _OrderType;
    [PXDBString(2, IsKey = true, IsFixed = true, InputMask = ">aa")]
    [PXDefault()]    
    public virtual String OrderType
    {
      get
      {
        return this._OrderType;
      }
      set
      {
        this._OrderType = value;
      }
    }
    #endregion
    #region OrderNbr
    public abstract class orderNbr : PX.Data.IBqlField
    {
    }
    protected String _OrderNbr;
    [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = "")]
    [PXDefault()]        
    public virtual String OrderNbr
    {
      get
      {
        return this._OrderNbr;
      }
      set
      {
        this._OrderNbr = value;
      }
    }
    #endregion
  }

  public class RQOrderCategory
  {
    public const string PO = "PO";
    public const string SO = "SO";

    public class po : Constant<string> { public po() : base(PO) { } }
    public class so : Constant<string> { public so() : base(SO) { } }
  }
}