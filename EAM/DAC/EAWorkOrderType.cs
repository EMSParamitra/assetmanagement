using System;
using PX.Data;

namespace EAM
{
    [Serializable]
    public class EAWorkOrderType : IBqlTable
    {
        #region EQTypeID
        public abstract class wOTypeID : PX.Data.IBqlField { }
        protected string _WOTypeID;
        [PXDBString(15, IsKey = true, IsUnicode = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Work Order Type", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<EAWorkOrderType.wOTypeID>))]
        public virtual string WOTypeID
        {
            get { return this._WOTypeID; }
            set { this._WOTypeID = value; }
        }
        #endregion

        #region Description
        public abstract class description : IBqlField { }
        protected string _Description;
        [PXDBString(60, IsUnicode = true)]
        [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }
        #endregion
    }
}