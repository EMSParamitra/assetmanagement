using System;
using PX.Data;

namespace EAM
{
  [Serializable]
  public class EAEquipmentType : IBqlTable
  {
    #region EQTypeID
    public abstract class eQTypeID : PX.Data.IBqlField { }
    protected string _EQTypeID;
    [PXDBString(15, IsKey = true, IsUnicode = true)]
    [PXDefault()]
    [PXUIField(DisplayName = "Equipment Type", Visibility = PXUIVisibility.SelectorVisible)]
    [PXSelector(typeof(Search<EAEquipmentType.eQTypeID>))]
    public virtual string EQTypeID 
    { 
      get { return this._EQTypeID; } 
      set { this._EQTypeID = value; } 
    }
    #endregion

    #region Description
    public abstract class description : IBqlField { }
    protected string _Description;  
    [PXDBString(60, IsUnicode = true)]
    [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
    public virtual string Description
    { 
      get { return this._Description; } 
      set { this._Description = value; } 
    }
    #endregion
  }
}