using System;
using PX.Data;
using PX.Data.EP;
using PX.Common;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.TX;
using PX.Objects.EP;
using PX.Objects.CM;
using PX.TM;

namespace EAM
{
  [Serializable]
  public class EAEquipmentClass : IBqlTable
  {
    #region EQClassID
    public abstract class eQClassID : PX.Data.IBqlField { }
    [PXDBIdentity]
    [PXUIField(Visible = false, Visibility = PXUIVisibility.Invisible)]
    public virtual int? EQClassID { get; set; }
    #endregion

    #region EQClassCD
    public abstract class eQClassCD : PX.Data.IBqlField { }
    protected String _EQClassCD;
    [PXDefault()]
    [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = "")]
    [PXUIField(DisplayName = "Equipment Class", Visibility = PXUIVisibility.SelectorVisible)]
    [PXSelector(typeof(EAEquipmentClass.eQClassCD), DescriptionField = typeof(EAEquipmentClass.descr))]
    public virtual string EQClassCD 
    { 
      get { return this._EQClassCD; } 
      set { this._EQClassCD = value; } 
    }
    #endregion

    #region Descr
    public abstract class descr : PX.Data.IBqlField { }
    [PXDBString(60, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
    [PXFieldDescription]
    public virtual string Descr { get; set; }
    #endregion
      
    #region Active

    public abstract class active : IBqlField { }
    protected Boolean? _Active;
    [PXDBBool]
    [PXDefault(true)]
    [PXUIField(DisplayName = "Active")]
    public virtual Boolean? Active
    {
      get { return this._Active; }
      set { this._Active = value; }
    }
    #endregion

    #region EQTypeID
    public abstract class eQTypeID : PX.Data.IBqlField { }
    [PXDBString(10, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Equipment Type", Visibility = PXUIVisibility.SelectorVisible)]
    [PXSelector(typeof(Search<EAEquipmentType.eQTypeID>), DescriptionField = typeof(EAEquipmentType.description))]
    public virtual string EQTypeID { get; set; }
    #endregion

    #region EQNumberingID
    public abstract class eQNumberingID : IBqlField { }
    protected String _EQNumberingID;
    [PXDBString(10, IsUnicode = true)]
    [PXSelector(typeof(Search<Numbering.numberingID>), DescriptionField = typeof(Numbering.descr))]
    [PXUIField(DisplayName = "Equipment Numbering Sequence")]
    public virtual string EQNumberingID 
    { get { return this._EQNumberingID; }
      set { this._EQNumberingID = value; } 
    }
    
    #endregion

    #region EQAccountID
    public abstract class eQAccountID : PX.Data.IBqlField
    {
    }
    protected Int32? _EQAccountID;
    [PXDefault(typeof(Search<EAEquipmentClass.eQAccountID>))]
    [Account(DisplayName = "Equipment Account", Visibility = PXUIVisibility.Visible, DescriptionField = typeof(Account.description))]
    public virtual Int32? EQAccountID
    {
      get
      {
        return this._EQAccountID;
      }
      set
      {
        this._EQAccountID = value;
      }
    }
    #endregion

    #region EQSubID
    public abstract class eQSubID : IBqlField {}
    protected Int32? _EQSubID;
    [PXDefault]
    [SubAccount(typeof(EAEquipmentClass.eQAccountID), DisplayName = "Equipment Sub.", Visibility = PXUIVisibility.Visible, DescriptionField = typeof(Sub.description))]
    public virtual Int32? EQSubID
    { 
      get{ return this._EQSubID; } 
      set{ this._EQSubID = value; }
    }
    #endregion

    #region CreatedByID
    public abstract class createdByID : PX.Data.IBqlField
    {
    }
    protected Guid? _CreatedByID;
    [PXDBCreatedByID()]
    public virtual Guid? CreatedByID
    {
      get
      {
        return this._CreatedByID;
      }
      set
      {
        this._CreatedByID = value;
      }
    }
    #endregion

    #region CreatedByScreenID
    public abstract class createdByScreenID : PX.Data.IBqlField
    {
    }
    protected string _CreatedByScreenID;
    [PXDBCreatedByScreenID()]
    public virtual string CreatedByScreenID
    {
      get
      {
        return this._CreatedByScreenID;
      }
      set
      {
        this._CreatedByScreenID = value;
      }
    }
    #endregion

    #region CreatedDateTime
    public abstract class createdDateTime : PX.Data.IBqlField
    {
    }
    protected DateTime? _CreatedDateTime;
    [PXDBCreatedDateTime]
    [PXUIField(DisplayName = "Created Date", Enabled = false, IsReadOnly = true)]
    public virtual DateTime? CreatedDateTime
    {
      get
      {
        return this._CreatedDateTime;
      }
      set
      {
        this._CreatedDateTime = value;
      }
    }
    #endregion

    #region LastModifiedByID
    public abstract class lastModifiedByID : PX.Data.IBqlField
    {
    }
    protected Guid? _LastModifiedByID;
    [PXDBLastModifiedByID()]
    public virtual Guid? LastModifiedByID
    {
      get
      {
        return this._LastModifiedByID;
      }
      set
      {
        this._LastModifiedByID = value;
      }
    }
    #endregion

    #region LastModifiedByScreenID
    public abstract class lastModifiedByScreenID : PX.Data.IBqlField
    {
    }
    protected string _LastModifiedByScreenID;
    [PXDBLastModifiedByScreenID()]
    public virtual string LastModifiedByScreenID
    {
      get
      {
        return this._LastModifiedByScreenID;
      }
      set
      {
        this._LastModifiedByScreenID = value;
      }
    }
    #endregion

    #region LastModifiedDateTime
    public abstract class lastModifiedDateTime : PX.Data.IBqlField
    {
    }
    protected DateTime? _LastModifiedDateTime;
    [PXDBLastModifiedDateTime]
    [PXUIField(DisplayName = "Last Modified Date", Enabled = false, IsReadOnly = true)]
    public virtual DateTime? LastModifiedDateTime
    {
      get
      {
        return this._LastModifiedDateTime;
      }
      set
      {
        this._LastModifiedDateTime = value;
      }
    }
    #endregion

    #region Tstamp
    public abstract class Tstamp : PX.Data.IBqlField
    {
    }
    protected byte[] _tstamp;
    [PXDBTimestamp()]
    public virtual byte[] tstamp
    {
      get
      {
        return this._tstamp;
      }
      set
      {
        this._tstamp = value;
      }
    }
    #endregion

    #region NoteID
    public abstract class noteID : PX.Data.IBqlField
    {
    }
    protected Guid? _NoteID;
    [PXNote(DescriptionField = typeof(EAEquipmentClass.eQClassCD),
    Selector = typeof(EAEquipmentClass.eQClassCD))]
    public virtual Guid? NoteID
    {
      get
      {
        return this._NoteID;
      }
      set
      {
        this._NoteID = value;
      }
    }
    #endregion
  }
}