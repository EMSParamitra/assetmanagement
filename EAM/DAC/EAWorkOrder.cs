using System;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.FA;
using PX.Objects.IN;
using PX.Objects.GL;
using PX.Objects.CS;
using PX.Objects.PO;
using PX.Objects.EP;
using PX.SM;
using PX.Data.ReferentialIntegrity.Attributes;


namespace EAM
{
    [Serializable]
    public class EAWorkOrder : IBqlTable
    {
        #region EAWorkOrderID
        public abstract class eAWorkOrderID : PX.Data.IBqlField { }
        protected String _EAWorkOrderID;
        [PXDefault]
        [PXDBString(10, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Work Order Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
        //[WO.WONumbering()]
        [AutoNumber(typeof(EAWorkOrderClass.wONumberingID), typeof(AccessInfo.businessDate))]
        [EASelectorWorkOrderNbr]
        //[WO.WorkOrderNbr(typeof(Search<EAWorkOrder.eAWorkOrderID, 
        //    Where<EAWorkOrder.eAWorkOrderID, Equal<Optional<EAWorkOrder.eAWorkOrderID>>>>), Filterable = true)]
        public virtual string EAWorkOrderID
        {
            get { return this._EAWorkOrderID; }
            set { this._EAWorkOrderID = value; }
        }
        #endregion

        #region EquipmentID
        [PXDBInt()]
        [PXUIField(DisplayName = "Equipment Nbr.")]
        [PXSelector(typeof(Search<EAEquipment.eAEquipmentID>), SubstituteKey = typeof(EAEquipment.eAEquipmentCD), DescriptionField = typeof(EAEquipment.description))]
        public virtual int? EquipmentID { get; set; }
        public abstract class equipmentID : IBqlField { }
        #endregion

        #region Description
        public abstract class description : IBqlField { }
        protected String _Description;
        [PXDBString(255, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Description")]
        public virtual String Description 
        { 
            get { return this._Description; }
            set { this._Description = value; }
        }
        #endregion

        #region Status
        public abstract class status : IBqlField { }
        protected string _Status;
        [PXDBString(1, IsFixed = true)]
        [ListField_WorkOrder_Status.ListAtrribute]
        [PXDefault("O")]
        [PXUIField(DisplayName = "Status", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string Status
        {
            get { return this._Status; }
            set { this._Status = value; }
        }
        #endregion

        #region CustomerID
        [PXDBInt()]
        [PXUIField(DisplayName = "Customer")]
        [EASelectorCustomer]
        //[PXDefault(typeof(Search<EAEquipment.ownerID, Where<EAEquipment.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual int? CustomerID { get; set; }
        public abstract class customerID : IBqlField, IBqlOperand { }
        #endregion

        #region Acctcd
        [PXDBString(20, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Customer Contact")]
        public virtual string Acctcd { get; set; }
        public abstract class acctcd : IBqlField { }
        #endregion

        #region Phone
        public abstract class phone : IBqlField { }
        protected String _Phone;
        [PXDBString(15, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Customer Contact Telp")]
        //[PXDefault(typeof(Search2<EACustomerHistory.phone, 
        //    InnerJoin<EAEquipment, On<EACustomerHistory.eAEquipmentID, Equal<EAEquipment.eAEquipmentID>,
        //    And<EACustomerHistory.revisionID, Equal<EAEquipment.ownerRevID>>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual String Phone 
        {
            get { return this._Phone;}
            set { this._Phone = value;} 
        }
        #endregion

        #region ContactPerson
        [PXDBString(20, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Contact Person")]
        public virtual string ContactPerson { get; set; }
        public abstract class contactPerson : IBqlField { }
        #endregion

        #region ContactPersonTelp
        [PXDBString(15, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Contact Person Telp")]
        public virtual string ContactPersonTelp { get; set; }
        public abstract class contactPersonTelp : IBqlField { }
        #endregion

        #region WOType
        [PXDBString(15, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "WO Type")]
        [PXSelector(typeof(Search<EAWorkOrderType.wOTypeID>), DescriptionField = typeof(EAWorkOrderType.description))]
        public virtual string WOType { get; set; }
        public abstract class wOType : IBqlField { }
        #endregion

        #region WOClassID
        [PXDBInt]
        [PXUIField(DisplayName = "Work Order Class")]
        [PXDefault()]
        [EASelectorWOClass]
        public virtual int? WOClassID { get; set; }
        public abstract class wOClassID : IBqlField, IBqlOperand { }
        #endregion

        #region JobType
        public abstract class jobType : ListField_JobType { }
        protected string _JobType;
        [PXDBString(3, IsFixed = true)]
        [ListField_JobType.ListAtrribute]
        [PXDefault("ASY")]
        [PXUIField(DisplayName = "Job Type")]
        public virtual string JobType
        {
            get { return this._JobType; }
            set { this._JobType = value; }
        }
        #endregion

        #region LicPlateNbr
        public abstract class licPlateNbr : IBqlField { }
        protected String _LicPlateNbr;
        [PXDBString(10, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Licensed Plate Nbr.")]
        [PXDefault(typeof(Search<EAEquipment.licPlateNbr, 
            Where<EAEquipment.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual string LicPlateNbr 
        {
            get { return this._LicPlateNbr;}
            set { this._LicPlateNbr = value;} 
        }
        #endregion

        #region VinNbr
        public abstract class vinNbr : IBqlField { }
        protected String _VinNbr;
        [PXDBString(20, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "VIN Nbr.")]
        [PXDefault(typeof(Search<EAEquipment.vinNbr,
            Where<EAEquipment.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual string VinNbr 
        { 
            get{ return this._VinNbr; } 
            set{ this._VinNbr = value;} 
        }
        #endregion

        #region EngineNbr
        public abstract class engineNbr : IBqlField { }
        protected String _EngineNbr;
        [PXDBString(30, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Engine Nbr.")]
        [PXDefault(typeof(Search<EAEquipment.engineNo,
            Where<EAEquipment.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual string EngineNbr
        {
            get { return this._EngineNbr;}
            set { this._EngineNbr = value;} 
        }
        #endregion

        #region Model
        public abstract class model : IBqlField { }
        protected String _Model;
        [PXDBString(15, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Model")]
        [PXDefault(typeof(Search<EAEquipment.inventoryID,
            Where<EAEquipment.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual String Model 
        {
            get { return this._Model;}
            set { this._Model = value;} 
        }
        #endregion

        #region Type
        public abstract class type : IBqlField { }
        protected String _Type;
        [PXDBString(10, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Equipment Type")]
        [PXDefault(typeof(Search<EAEquipment.eQTypeID,
            Where<EAEquipment.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual String Type 
        {
            get { return this._Type;}
            set { this._Type = value;} 
        }
        
        #endregion

        #region KMReading
        public abstract class kMReading : IBqlField { }
        protected Decimal? _KMReading;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "KM Reading")]
        [PXDefault(TypeCode.Decimal, "0.0")]
        public virtual Decimal? KMReading 
        {
            get { return this._KMReading;}
            set { this._KMReading = value;} 
        }
        #endregion

        #region StartDate
        public abstract class startDate : IBqlField { }
        protected DateTime? _StartDate;
        [PXDBDateAndTime(DisplayNameDate = "Start Date", DisplayNameTime = "Start Time", PreserveTime = true, UseTimeZone = true)]
        [PXUIField(DisplayName = "Start Date", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual DateTime? StartDate 
        {
            get { return this._StartDate;}
            set
            {
                this._StartDate = value;
                this._StartDateUTC = value;
            }
        }
        #endregion

        #region StartDateUTC
        public abstract class startDateUTC : IBqlField { }
        protected DateTime? _StartDateUTC;
        [PXDBDateAndTime(DisplayNameDate = "Start Date", DisplayNameTime = "Start Time", PreserveTime = true, UseTimeZone = false)]
        [PXUIField(DisplayName = "Start Date", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual DateTime? StartDateUTC
        {
            get;
            set;
        }
        #endregion

        #region FinishDate
        public abstract class finishDate : IBqlField { }
        protected DateTime? _FinishDate;
        [PXDBDateAndTime(DisplayNameDate = "Finish Date", DisplayNameTime = "Finish Time", PreserveTime = true, UseTimeZone = true)]
        [PXUIField(DisplayName = "Finish Date", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual DateTime? FinishDate
        {
            get { return this._FinishDate; }
            set
            {
                this._FinishDate = value;
            }
        }
        #endregion

        #region StartTime
        [PXDBDate()]
        [PXUIField(DisplayName = "Start Time")]
        public virtual DateTime? StartTime { get; set; }
        public abstract class startTime : IBqlField { }
        #endregion

        #region FinishTime
        [PXDBDate()]
        [PXUIField(DisplayName = "Finish Time")]
        public virtual DateTime? FinishTime { get; set; }
        public abstract class finishTime : IBqlField { }
        #endregion

        #region TakenBy
        public abstract class takenBy : PX.Data.IBqlField { }
        protected int? _TakenBy;
        [PXDBInt]
        [PXUIField(DisplayName = "Taken By")]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        //[PXFormula(typeof(Selector<PX.Objects.EP.EPEmployee.bAccountID, EPEmployee.userID>))]
        public virtual int? TakenBy
        {
            get
            {
                return this._TakenBy;
            }
            set
            {
                this._TakenBy = value;
            }
        }
        #endregion

        #region DoneBy
        public abstract class doneBy : IBqlField { }
        protected int? _DoneBy;
        [PXDBInt]
        [PXUIField(DisplayName = "Checked by")]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        public virtual int? DoneBy 
        {
            get { return this._DoneBy;}
            set { this._DoneBy = value;} 
        }
        #endregion

        #region JobInstruction
        protected String _JobInstruction;
        [PXDBText(IsUnicode = true)]
        [PXUIField(DisplayName = "Job Instruction")]
        public virtual string JobInstruction
        {
            get
            {
                return this._JobInstruction;
            }
            set
            {
                this._JobInstruction = value;
                _plainText = null;
            }
        }
        public abstract class jobInstruction : IBqlField { }
        #endregion

        #region JobInstructionAsPlainText
        public abstract class jobInstructionAsPlainText : IBqlField { }
        private string _plainText;
        [PXString(IsUnicode = true)]
        [PXUIField(Visible = false)]
        public virtual String JobInstructionAsPlainText
        {
            get
            {
                return _plainText ?? (_plainText = PX.Data.Search.SearchService.Html2PlainText(this.JobInstruction));
            }
        }
        #endregion

        #region Comment
        protected String _Comment;
        [PXDBText(IsUnicode = true)]
        [PXUIField(DisplayName = "Comment")]
        public virtual string Comment
        {
            get
            {
                return this._Comment;
            }
            set
            {
                this._Comment = value;
                _commentPlainText = null;
            }
        }
        public abstract class comment : IBqlField { }
        #endregion

        #region CommentAsPlainText
        public abstract class commentAsPlainText : IBqlField { }
        private string _commentPlainText;
        [PXString(IsUnicode = true)]
        [PXUIField(Visible = false)]
        public virtual String CommentAsPlainText
        {
            get
            {
                return _commentPlainText ?? (_commentPlainText = PX.Data.Search.SearchService.Html2PlainText(this.Comment));
            }
        }
        #endregion

        #region WOLineCntr
        public abstract class wOLineCntr : PX.Data.IBqlField
        {
        }

        [PXDBInt]
        [PXDefault(0)]
        public virtual Int32? WOLineCntr { get; set; }
        #endregion

        #region CuryID
        public abstract class curyID : PX.Data.IBqlField
        {
        }
        protected String _CuryID;
        [PXDBString(5, IsUnicode = true, InputMask = ">LLLLL")]
        [PXUIField(DisplayName = "Currency", Visibility = PXUIVisibility.SelectorVisible)]
        [PXDefault(typeof(Search<Company.baseCuryID>))]
        [PXSelector(typeof(Currency.curyID))]
        public virtual String CuryID
        {
            get
            {
                return this._CuryID;
            }
            set
            {
                this._CuryID = value;
            }
        }
        #endregion

        #region CuryInfoID
        public abstract class curyInfoID : PX.Data.IBqlField
        {
        }
        protected Int64? _CuryInfoID;
        [PXDBLong()]
        [CurrencyInfo()]
        public virtual Int64? CuryInfoID
        {
            get
            {
                return this._CuryInfoID;
            }
            set
            {
                this._CuryInfoID = value;
            }
        }
        #endregion

        #region CuryOrderTotal
        public abstract class curyOrderTotal : PX.Data.IBqlField
        {
        }
        protected Decimal? _CuryOrderTotal;
        [PXDBCurrency(typeof(EAWorkOrder.curyInfoID), typeof(EAWorkOrder.orderTotal))]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Order Total")]
        public virtual Decimal? CuryOrderTotal
        {
            get
            {
                return this._CuryOrderTotal;
            }
            set
            {
                this._CuryOrderTotal = value;
            }
        }
        #endregion

        #region OrderTotal
        public abstract class orderTotal : PX.Data.IBqlField
        {
        }
        protected Decimal? _OrderTotal;
        [PXDBDecimal(4)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        public virtual Decimal? OrderTotal
        {
            get
            {
                return this._OrderTotal;
            }
            set
            {
                this._OrderTotal = value;
            }
        }
        #endregion

        #region CuryLineTotal
        public abstract class curyLineTotal : PX.Data.IBqlField
        {
        }
        protected Decimal? _CuryLineTotal;
        [PXDBCurrency(typeof(EAWorkOrder.curyInfoID), typeof(EAWorkOrder.lineTotal))]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Line Total")]
        public virtual Decimal? CuryLineTotal
        {
            get
            {
                return this._CuryLineTotal;
            }
            set
            {
                this._CuryLineTotal = value;
            }
        }
        #endregion

        #region LineTotal
        public abstract class lineTotal : PX.Data.IBqlField
        {
        }
        protected Decimal? _LineTotal;
        [PXDBDecimal(4)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        public virtual Decimal? LineTotal
        {
            get
            {
                return this._LineTotal;
            }
            set
            {
                this._LineTotal = value;
            }
        }
        #endregion

        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected Boolean? _Hold;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Hold")]
        public virtual Boolean? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }
        #endregion

        #region Billed
        public abstract class billed : PX.Data.IBqlField
        {
        }
        protected Boolean? _Billed;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Billed", Visibility = PXUIVisibility.Visible)]
        public virtual Boolean? Billed
        {
            get
            {
                return this._Billed;
            }
            set
            {
                this._Billed = value;
            }
        }
        #endregion

        #region CSOrderNbr
        public abstract class cSOrderNbr : PX.Data.IBqlField
        {
        }
        protected String _CSOrderNbr;
        [PXDBString(15, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "CS Nbr.", Enabled = false)]
        public virtual String CSOrderNbr
        {
            get
            {
                return this._CSOrderNbr;
            }
            set
            {
                this._CSOrderNbr = value;
            }
        }
        #endregion

    }

    public class WO
    {
        public class WorkOrderNbrAttribute : PXSelectorAttribute
        {
            public WorkOrderNbrAttribute(Type SearchType)
                : base(SearchType,
                typeof(EAWorkOrder.eAWorkOrderID),
                typeof(EAWorkOrder.description),
                typeof(EAWorkOrder.wOType),
                typeof(EAWorkOrder.wOClassID),
                typeof(EAWorkOrder.equipmentID),
                typeof(EAWorkOrder.customerID),
                typeof(EAWorkOrder.status))
            {
            }
        }
		
        public class WONumberingAttribute : AutoNumberAttribute
        {
            public WONumberingAttribute()
                : base(typeof(Search<EAWorkOrderClass.wONumberingID, Where<EAWorkOrderClass.wOClassID, Equal<Current<EAWorkOrder.wOClassID>>>>), typeof(AccessInfo.businessDate))
            { ; }
        }
    }
}