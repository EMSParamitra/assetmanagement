using System;
using PX.Data;
using PX.Objects.GL;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.CR.MassProcess;

namespace EAM
{
  [Serializable]
  public class EACustomerHistory : IBqlTable
  {
    #region EAEquipmentID
    public abstract class eAEquipmentID : PX.Data.IBqlField
    {
    }
    protected Int32? _EAEquipmentID;
    [PXDBInt(IsKey = true)]
    [PXUIField(Visible = false, Visibility = PXUIVisibility.Invisible)]
    [PXParent(typeof(Select<EAEquipment, Where<EAEquipment.eAEquipmentID, Equal<Current<EACustomerHistory.eAEquipmentID>>>>))]
    [PXDBLiteDefault(typeof(EAEquipment.eAEquipmentID))]
    public virtual Int32? EAEquipmentID
    {
      get
      {
        return this._EAEquipmentID;
      }
      set
      {
        this._EAEquipmentID = value;
      }
    }
    #endregion
    #region TransactionType
    public abstract class transactionType : PX.Data.IBqlField
    {
      public class ListAttribute : PXStringListAttribute
      {
        public ListAttribute()
          : base(
          new string[] { ChangeOwner, UpdateData },
          new string[] { "Change Owner", "Update Data" }) { ; }
      }

      public const string ChangeOwner = "C";
      public const string UpdateData = "U";

      public class changeOwner : Constant<string>
      {
        public changeOwner() : base(ChangeOwner) { ;}
      }
      public class updateData : Constant<string>
      {
        public updateData() : base(UpdateData) { ;}
      }
    }
    protected String _TransactionType;
    [PXDBString(1, IsFixed = true)]
    [PXDefault(transactionType.ChangeOwner)]
    [transactionType.List()]
    [PXUIField(DisplayName = "Transaction Type")]
    public virtual String TransactionType
    {
      get
      {
        return this._TransactionType;
      }
      set
      {
        this._TransactionType = value;
      }
    }
    #endregion
    #region TransactionDate
    public abstract class transactionDate : PX.Data.IBqlField
    {
    }
    protected DateTime? _TransactionDate;
    [PXDBDate()]
    [PXUIField(DisplayName = "Transaction Date")]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    public virtual DateTime? TransactionDate
    {
      get
      {
        return this._TransactionDate;
      }
      set
      {
        this._TransactionDate = value;
      }
    }
    #endregion
    #region RevisionID
    public abstract class revisionID : PX.Data.IBqlField
    {
    }
    protected Int32? _RevisionID;
    [PXDBInt(IsKey = true)]
    [PXDefault(0)]
    public virtual Int32? RevisionID
    {
      get
      {
        return this._RevisionID;
      }
      set
      {
        this._RevisionID = value;
      }
    }
    #endregion
    #region PeriodID
    public abstract class periodID : IBqlField
    {
    }
    protected string _PeriodID;
    [PeriodID]
    public virtual string PeriodID
    {
      get
      {
        return _PeriodID;
      }
      set
      {
        _PeriodID = value;
      }
    }
    #endregion
    #region OwnerType
    [PXDBString(2, IsFixed = true)]
    [PXUIField(DisplayName = "Owner Type")]
    [PXDefault("OW", PersistingCheck = PXPersistingCheck.Nothing)]
    [ListField_OwnerType_Equipment.ListAtrribute]
    public virtual string OwnerType { get; set; }
    public abstract class ownerType : IBqlField, IBqlOperand { }
    #endregion
    #region OwnerID
    [PXDBInt()]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Customer")]
    [EASelectorCustomer]
    public virtual int? OwnerID { get; set; }
    public abstract class ownerID : IBqlField, IBqlOperand { }
    #endregion
    #region LocationType
    [PXDBString(2, IsFixed = true)]
    [PXDefault("CU", PersistingCheck = PXPersistingCheck.Nothing)]
    [PXUIField(DisplayName = "Location Type")]
    [ListField_LocationType.ListAtrribute]
    public virtual string LocationType { get; set; }
    public abstract class locationType : ListField_LocationType { }
    #endregion
    #region BranchLocationID
    [PXDBInt]
    [PXUIField(DisplayName = "Branch Location")]
    [PXSelector(typeof (Search<EABranchLocation.branchLocationID, Where<EABranchLocation.branchID, Equal<Current<EAEquipment.branchID>>>>), DescriptionField = typeof (EABranchLocation.descr), SubstituteKey = typeof (EABranchLocation.branchLocationCD))]
    [PXFormula(typeof (PX.Data.Default<EAEquipment.branchID>))]
    public virtual int? BranchLocationID { get; set; }
    public abstract class branchLocationID : IBqlField, IBqlOperand { }
    #endregion
    #region CustomerLocationID
    [PX.Objects.CS.LocationID(typeof (Where<Location.bAccountID, Equal<Current<EACustomerHistory.ownerID>>>), DescriptionField = typeof (Location.descr), DirtyRead = true, DisplayName = "Location")]
    [PXDefault(typeof (Search<PX.Objects.CR.BAccount.defLocationID, Where<PX.Objects.CR.BAccount.bAccountID, Equal<Current<EACustomerHistory.ownerID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    [PXFormula(typeof (PX.Data.Default<EACustomerHistory.ownerID>))]
    public virtual int? CustomerLocationID { get; set; }
    public abstract class customerLocationID : IBqlField, IBqlOperand { }
    #endregion
    #region IsDefaultAddress
    public abstract class isDefaultAddress : PX.Data.IBqlField
    {
    }
    protected Boolean? _IsDefaultAddress;
    [PXDBBool()]
    [PXUIField(DisplayName = "Default Customer Location", Visibility = PXUIVisibility.Visible)]
    [PXDefault(true)]
    public virtual Boolean? IsDefaultAddress
    {
        get
        {
            return this._IsDefaultAddress;
        }
        set
        {
            this._IsDefaultAddress = value;
        }
    }
    #endregion
    #region OverrideAddress
    public abstract class overrideAddress : PX.Data.IBqlField
    {
    }
    [PXBool()]
    [PXUIField(DisplayName = "Override Address", Visibility = PXUIVisibility.Visible)]
    public virtual Boolean? OverrideAddress
    {
        [PXDependsOnFields(typeof(isDefaultAddress))]
        get
        {
            return (bool?)(this._IsDefaultAddress == null ? this._IsDefaultAddress : this._IsDefaultAddress == false);
        }
        set
        {
            this._IsDefaultAddress = (bool?)(value == null ? value : value == false);
        }
    }
    #endregion
    #region AddressLine1
    public abstract class addressLine1 : PX.Data.IBqlField
    {
    }
    protected String _AddressLine1;
    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Address Line 1", Visibility = PXUIVisibility.SelectorVisible)]
    public virtual String AddressLine1
    {
        get
        {
            return this._AddressLine1;
        }
        set
        {
            this._AddressLine1 = value;
        }
    }
    #endregion
    #region AddressLine2
    public abstract class addressLine2 : PX.Data.IBqlField
    {
    }
    protected String _AddressLine2;
    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Address Line 2")]
    public virtual String AddressLine2
    {
        get
        {
            return this._AddressLine2;
        }
        set
        {
            this._AddressLine2 = value;
        }
    }
    #endregion
    #region AddressLine3
    public abstract class addressLine3 : PX.Data.IBqlField
    {
    }
    protected String _AddressLine3;
    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Address Line 3")]
    public virtual String AddressLine3
    {
        get
        {
            return this._AddressLine3;
        }
        set
        {
            this._AddressLine3 = value;
        }
    }
    #endregion
    #region Phone
    public abstract class phone : PX.Data.IBqlField { }
    protected String _Phone; 
    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "Phone", Visibility = PXUIVisibility.SelectorVisible)]
    [PhoneValidation()]
    [PXMassMergableField]
    public virtual String Phone 
    { 
        get { return this._Phone; }
        set { this._Phone = value; } 
    }
    #endregion
    #region EMail
    public abstract class eMail : PX.Data.IBqlField { }
    private string _eMail;

    [PXDBEmail]
    [PXUIField(DisplayName = "Email", Visibility = PXUIVisibility.SelectorVisible)]
    [PXMassMergableField]
    [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
    public virtual String EMail
    {
        get { return _eMail; }
        set { _eMail = value != null ? value.Trim() : null; }
    }
    #endregion
    #region City
    public abstract class city : PX.Data.IBqlField
    {
    }
    protected String _City;
    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "City", Visibility = PXUIVisibility.SelectorVisible)]
    public virtual String City
    {
        get
        {
            return this._City;
        }
        set
        {
            this._City = value;
        }
    }
    #endregion
    #region CountryID
    public abstract class countryID : PX.Data.IBqlField
    {
    }
    protected String _CountryID;
    //[PXDefault(typeof(Search<PX.Objects.GL.Branch.countryID, Where<PX.Objects.GL.Branch.branchID, Equal<Current<AccessInfo.branchID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
    [PXDBString(100)]
    [PXUIField(DisplayName = "Country")]
    [Country]
    public virtual String CountryID
    {
        get
        {
            return this._CountryID;
        }
        set
        {
            this._CountryID = value;
        }
    }
    #endregion
    #region State
    public abstract class state : PX.Data.IBqlField
    {
    }
    protected String _State;
    [PXDBString(50, IsUnicode = true)]
    [PXUIField(DisplayName = "State")]
    [PX.Objects.CR.State(typeof(EACustomerHistory.countryID))]
    public virtual String State
    {
        get
        {
            return this._State;
        }
        set
        {
            this._State = value;
        }
    }
    #endregion
    #region PostalCode
    public abstract class postalCode : PX.Data.IBqlField
    {
    }
    protected String _PostalCode;
    [PXDBString(20)]
    [PXUIField(DisplayName = "Postal Code")]
    [PXZipValidation(typeof(Country.zipCodeRegexp), typeof(Country.zipCodeMask), countryIdField: typeof(EACustomerHistory.countryID))]
    public virtual String PostalCode
    {
        get
        {
            return this._PostalCode;
        }
        set
        {
            this._PostalCode = value;
        }
    }
    #endregion
    #region IsValidated
    public abstract class isValidated : PX.Data.IBqlField
    {
    }
    protected Boolean? _IsValidated;

    [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
    [PXDBBool()]
    [PX.Objects.CS.ValidatedAddress()]
    [PXUIField(DisplayName = "Validated", FieldClass = PX.Objects.CS.Messages.ValidateAddress)]
    public virtual Boolean? IsValidated
    {
        get
        {
            return this._IsValidated;
        }
        set
        {
            this._IsValidated = value;
        }
    }
    #endregion
    #region RefNbr
    [PXDBString(15, IsUnicode = true, InputMask = "")]
    [PXUIField(DisplayName = "Ref Nbr")]
    public virtual string RefNbr { get; set; }
    public abstract class refNbr : IBqlField { }
    #endregion
    #region Reason
    public abstract class reason : PX.Data.IBqlField
    {
    }
    protected String _Reason;
    /// <summary>
    /// The reason of the change of the asset location.
    /// The information field, which value is entered manually.
    /// </summary>
    [PXDBString(30, IsUnicode = true)]
    [PXUIField(DisplayName = "Reason")]
    public virtual String Reason
    {
      get
      {
        return this._Reason;
      }
      set
      {
        this._Reason = value;
      }
    }
    #endregion
    #region Tstamp
    public abstract class Tstamp : PX.Data.IBqlField
    {
    }
    protected Byte[] _tstamp;
    [PXDBTimestamp()]
    public virtual Byte[] tstamp
    {
      get
      {
        return this._tstamp;
      }
      set
      {
        this._tstamp = value;
      }
    }
    #endregion
    #region CreatedByID

    [PXDBCreatedByID()]
    public abstract class createdByID : PX.Data.IBqlField
    {
    }
    protected Guid? _CreatedByID;
    [PXDBCreatedByID()]
    public virtual Guid? CreatedByID
    {
      get
      {
        return this._CreatedByID;
      }
      set
      {
        this._CreatedByID = value;
      }
    }
    #endregion
    #region CreatedByScreenID

    public abstract class createdByScreenID : PX.Data.IBqlField
    {
    }
    protected String _CreatedByScreenID;
    [PXDBCreatedByScreenID()]
    public virtual String CreatedByScreenID
    {
      get
      {
        return this._CreatedByScreenID;
      }
      set
      {
        this._CreatedByScreenID = value;
      }
    }
    #endregion
    #region CreatedDateTime
    public abstract class createdDateTime : PX.Data.IBqlField
    {
    }
    protected DateTime? _CreatedDateTime;
    [PXDBCreatedDateTime()]
    public virtual DateTime? CreatedDateTime
    {
      get
      {
        return this._CreatedDateTime;
      }
      set
      {
        this._CreatedDateTime = value;
      }
    }
    #endregion
    #region LastModifiedByID

    public abstract class lastModifiedByID : PX.Data.IBqlField
    {
    }
    protected Guid? _LastModifiedByID;
    [PXDBLastModifiedByID]
    public virtual Guid? LastModifiedByID
    {
      get
      {
        return this._LastModifiedByID;
      }
      set
      {
        this._LastModifiedByID = value;
      }
    }
    #endregion
    #region LastModifiedByScreenID

    public abstract class lastModifiedByScreenID : PX.Data.IBqlField
    {
    }
    protected String _LastModifiedByScreenID;
    [PXDBLastModifiedByScreenID()]
    public virtual String LastModifiedByScreenID
    {
      get
      {
        return this._LastModifiedByScreenID;
      }
      set
      {
        this._LastModifiedByScreenID = value;
      }
    }
    #endregion
    #region LastModifiedDateTime
    public abstract class lastModifiedDateTime : PX.Data.IBqlField
    {
    }
    protected DateTime? _LastModifiedDateTime;
    [PXDBLastModifiedDateTime]
    [PXUIField(DisplayName = "Modification Date")]
    public virtual DateTime? LastModifiedDateTime
    {
      get
      {
        return this._LastModifiedDateTime;
      }
      set
      {
        this._LastModifiedDateTime = value;
      }
    }
    #endregion
  }
}