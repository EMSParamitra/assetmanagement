using System;
using PX.Data;

namespace EAM
{
  public class EAWorkOrderTypeMaint : PXGraph<EAWorkOrderTypeMaint, EAWorkOrder>
  {

    public PXSave<EAWorkOrderType> Save;
    public PXCancel<EAWorkOrderType> Cancel;


    public PXSelect<EAWorkOrderType> WorkOrderTypes;


  }
}