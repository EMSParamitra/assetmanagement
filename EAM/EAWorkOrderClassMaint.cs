using System;
using PX.Data;
using PX.Data.Maintenance;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.IN;

namespace EAM
{
  public class EAWorkOrderClassMaint : PXGraph<EAWorkOrderClassMaint, EAWorkOrderClass>
  {

    public PXSelect<EAWorkOrderClass> WorkOrderClass;
    public PXSelect<EAWorkOrderClass, Where<EAWorkOrderClass.wOClassCD, Equal<Current<EAWorkOrderClass.wOClassCD>>>> CurrentWorkOrderClass;

  }
}