using System;
using PX.Data;
using PX.Objects.IN;
using PX.Objects.SO;
using PX.Objects.PO;
using PX.Objects.CM;
using PX.Objects.CS;
using System.Collections;
using System.Collections.Generic;

namespace EAM
{
    public class EAWorkOrderEntry : PXGraph<EAWorkOrderEntry, EAWorkOrder>
    {

        public PXSelect<EAWorkOrder> WorkOrders;
        public PXSelect<EAWorkOrder, Where<EAWorkOrder.eAWorkOrderID, Equal<Current<EAWorkOrder.eAWorkOrderID>>>> CurrentWorkOrder;
        public PXSelect<EAWorkOrderClass, Where<EAWorkOrderClass.wOClassID, Equal<Current<EAWorkOrder.wOClassID>>>> WOClass;
        public PXSelect<EAEquipment, Where<EAEquipment.eAEquipmentID, Equal<Current<EAWorkOrder.equipmentID>>>> Equipments;
        public PXSelectJoin<EACustomerHistory,
            InnerJoin<EAEquipment, On<EACustomerHistory.eAEquipmentID, Equal<EAEquipment.eAEquipmentID>,
            And<EACustomerHistory.revisionID, Equal<EAEquipment.ownerRevID>>>>,
            Where<EACustomerHistory.eAEquipmentID, Equal<Optional<EAWorkOrder.equipmentID>>>> EqCustomer;
        public PXSetup<EAWorkOrderClass> WOSetup;
        public PXSelectJoin<EAWorkOrderLine, 
            InnerJoin<EAWorkOrder, On<EAWorkOrderLine.eAWorkOrderID, Equal<EAWorkOrder.eAWorkOrderID>>>,
            Where<EAWorkOrderLine.eAWorkOrderID, Equal<Current<EAWorkOrder.eAWorkOrderID>>>> WOParts;
        public PXSelect<EAWOOrder, Where<EAWOOrder.eAWorkOrderNbr, Equal<Optional<EAWorkOrder.eAWorkOrderID>>>> ReqOrders;
        public PXSelect<CurrencyInfo, Where<CurrencyInfo.curyInfoID, Equal<Optional<EAWorkOrder.curyInfoID>>>> currencyinfo;
        public PXSelectJoin<SOOrder,
                InnerJoin<EAWOOrder,
                             On<EAWOOrder.orderCategory, Equal<RQOrderCategory.so>,
                             And<EAWOOrder.orderType, Equal<SOOrder.orderType>,
                            And<EAWOOrder.orderNbr, Equal<SOOrder.orderNbr>>>>>,
            Where<EAWOOrder.eAWorkOrderNbr, Equal<Optional<EAWorkOrder.eAWorkOrderID>>>> SOOrders;

        public PXAction<EAWorkOrder> Act;
        [PXButton(Tooltip = "Actions")]
        [PXUIField(DisplayName = "ACTIONS", MapEnableRights = PXCacheRights.Delete, MapViewRights = PXCacheRights.Delete)]
        protected IEnumerable act(PXAdapter adapter)
        {
            return adapter.Get();
        }
        
        public EAWorkOrderEntry()
        {
            EAWorkOrderClass setup = WOSetup.Current;
            Act.MenuAutoOpen = true;
            Act.AddMenuAction(PrintJobCard);
            Act.AddMenuAction(CreateCSOrder);
        }

        public PXAction<EAM.EAWorkOrder> PrintJobCard;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Print Job Card")]

        protected void printJobCard()
        {
            EAWorkOrder doc = WorkOrders.Current;
            if(doc.EAWorkOrderID != null)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters["WONbr"] = doc.EAWorkOrderID;
                throw new PXReportRequiredException(parameters, "EA611000", null);
            }
        }

        protected virtual void EAWorkOrder_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            EAWorkOrder row = (EAWorkOrder)e.Row;

            if (row.Status == "C")
            {
                WOParts.Cache.AllowUpdate = false;
                WOParts.Cache.AllowInsert = false;
                WOParts.Cache.AllowDelete = false;
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.equipmentID>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.wOClassID>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.wOType>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.kMReading>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.startDate>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.finishDate>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.takenBy>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.doneBy>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.jobInstruction>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.comment>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.description>(sender, row, false);
            } 
            else
            {
                WOParts.Cache.AllowUpdate = true;
                WOParts.Cache.AllowInsert = true;
                WOParts.Cache.AllowDelete = true;
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.equipmentID>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.wOClassID>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.wOType>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.kMReading>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.startDate>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.finishDate>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.takenBy>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.doneBy>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.jobInstruction>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.comment>(sender, row, true);
                PXUIFieldAttribute.SetEnabled<EAWorkOrder.description>(sender, row, true);
            }
        }
        
        protected void EAWorkOrder_EquipmentID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            if (e.Row == null) return;

            EAWorkOrder row = (EAWorkOrder)e.Row;

            EACustomerHistory eqCust = EqCustomer.Select();
            EAEquipment eq = Equipments.Select();

            if(eqCust.OwnerID != null)
            {
                sender.SetValue<EAWorkOrder.customerID>(e.Row, eqCust.OwnerID);
                sender.SetValue<EAWorkOrder.phone>(e.Row, eqCust.Phone);
                sender.SetValue<EAWorkOrder.vinNbr>(e.Row, eq.VinNbr);
                sender.SetValue<EAWorkOrder.licPlateNbr>(e.Row, eq.LicPlateNbr);
                sender.SetValue<EAWorkOrder.engineNbr>(e.Row, eq.EngineNo);
                sender.SetValue<EAWorkOrder.model>(e.Row, eq.InventoryID);
            }
           
        }

        protected void EAWorkOrderLine_InventoryID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            if (e.Row == null) return;

            EAWorkOrderLine row = (EAWorkOrderLine)e.Row;
            InventoryItem inventoryItemRow = GetInventoryItemRow(sender.Graph, row.InventoryID);

            sender.SetValue<EAWorkOrderLine.uOM>(e.Row, inventoryItemRow.SalesUnit);
            sender.SetValue<EAWorkOrderLine.wOLineDescription>(e.Row, inventoryItemRow.Descr);
            sender.SetValue<EAWorkOrderLine.curyUnitPrice>(e.Row, inventoryItemRow.BasePrice);
        }

        public PXAction<EAM.EAWorkOrder> CreateCSOrder;
        [PXButton(ImageKey = PX.Web.UI.Sprite.Main.DataEntry)]
        [PXUIField(DisplayName = "Create Cash Sales")]
        public virtual IEnumerable createCSOrder(PXAdapter adapter)
        {
            SOOrderEntry sograph = PXGraph.CreateInstance<SOOrderEntry>();
            List<SOOrder> list = new List<SOOrder>();
            foreach (EAWorkOrder item in adapter.Get<EAWorkOrder>())
            {
                EAWorkOrder result = item;
                EAWOOrder wo =
                PXSelectJoin<EAWOOrder,
                    InnerJoin<SOOrder,
                                 On<SOOrder.orderNbr, Equal<EAWOOrder.orderNbr>,
                                And<SOOrder.status, Equal<SOOrderStatus.open>>>>,
                    Where<EAWOOrder.eAWorkOrderNbr, Equal<Required<EAWOOrder.eAWorkOrderNbr>>,
                        And<EAWOOrder.orderCategory, Equal<RQOrderCategory.so>>>>
                        .Select(this, item.EAWorkOrderID);

                //EACustomerHistory cust =
                //PXSelectJoin<EACustomerHistory,
                //InnerJoin<EAWorkOrder,
                //    On<EACustomerHistory.eAEquipmentID, Equal<EAWorkOrder.equipmentID>>>,
                //        Where<EACustomerHistory.eAEquipmentID, Equal<Required<EAWorkOrder.equipmentID>>>>
                //        .Select(this, item.EquipmentID);

                EACustomerHistory chist = this.EqCustomer.Select();

                if (item.CustomerID != null && wo == null)
                {
                    this.WorkOrders.Current = item;

                    bool validateResult = true;
                    foreach (EAWorkOrderLine line in this.WOParts.Select(item.EAWorkOrderID))
                    {
                        if (!ValidateOpenState(line, PXErrorLevel.Error))
                            validateResult = false;
                    }
                    if (!validateResult)
                        throw new PXRowPersistingException(typeof(EAWOOrder).Name, item, "Unable to create orders, not all required fields are defined.");

                    sograph.TimeStamp = this.TimeStamp;
                    sograph.Document.Current = null;
                    foreach (PXResult<EAWorkOrderLine, InventoryItem> r in
                        PXSelectJoin<EAWorkOrderLine,
                        LeftJoin<InventoryItem,
                                    On<InventoryItem.inventoryID, Equal<EAWorkOrderLine.inventoryID>>>,
                        Where<EAWorkOrderLine.eAWorkOrderID, Equal<Required<EAWorkOrder.eAWorkOrderID>>>>.Select(this, item.EAWorkOrderID))
                    {
                        EAWorkOrderLine l = r;
                        InventoryItem i = r;

                        if (sograph.Document.Current == null)
                        {
                            SOOrder order = (SOOrder)sograph.Document.Cache.CreateInstance();
                            order.OrderType = "CS";
                            order = sograph.Document.Insert(order);
                            order = PXCache<SOOrder>.CreateCopy(sograph.Document.Search<SOOrder.orderNbr>(order.OrderNbr));
                            order.CustomerID = item.CustomerID;
                            order.CustomerLocationID = chist.CustomerLocationID;
                            order.ExtRefNbr = item.EAWorkOrderID;
                            order.OrderDesc = item.Description;
                            order = PXCache<SOOrder>.CreateCopy(sograph.Document.Update(order));
                            order.CuryID = item.CuryID;
                            order.CuryInfoID = CopyCurrenfyInfo(sograph, item.CuryInfoID);
                            sograph.Document.Update(order);
                            sograph.Save.Press();
                            order = sograph.Document.Current;
                            list.Add(order);

                            EAWOOrder link = new EAWOOrder();
                            link.OrderCategory = RQOrderCategory.SO;
                            link.OrderType = order.OrderType;
                            link.OrderNbr = order.OrderNbr;
                            this.ReqOrders.Insert(link);
                        }
                        SOLine line = (SOLine)sograph.Transactions.Cache.CreateInstance();
                        line.OrderType = sograph.Document.Current.OrderType;
                        line.OrderNbr = sograph.Document.Current.OrderNbr;
                        line = PXCache<SOLine>.CreateCopy(sograph.Transactions.Insert(line));
                        line.InventoryID = l.InventoryID;
                        if (line.InventoryID != null)
                            line.SubItemID = l.SubItemID;
                        line.UOM = l.UOM;
                        line.Qty = l.Quantity;
                        line.LocationID = l.LocationID;
                        if (l.SiteID != null)
                            line.SiteID = l.SiteID;

                        //if (l.IsUseMarkup == true)
                        //{
                        //    string curyID = item.CuryID;
                        //    decimal profit = (1m + l.MarkupPct.GetValueOrDefault() / 100m);
                        //    line.ManualPrice = true;
                        //    decimal unitPrice = l.EstUnitCost.GetValueOrDefault();
                        //    decimal curyUnitPrice = l.CuryEstUnitCost.GetValueOrDefault();

                        //    if (bidding != null && bidding.MinQty <= line.OrderQty && bidding.OrderQty >= line.OrderQty)
                        //    {
                        //        curyID = (string)Bidding.GetValueExt<RQBidding.curyID>(bidding);
                        //        unitPrice = bidding.QuoteUnitCost.GetValueOrDefault();
                        //        curyUnitPrice = bidding.CuryQuoteUnitCost.GetValueOrDefault();
                        //    }

                        //    if (curyID == sograph.Document.Current.CuryID)
                        //        line.CuryUnitPrice = curyUnitPrice * profit;
                        //    else
                        //    {
                        //        line.UnitPrice = unitPrice * profit;
                        //        PXCurrencyAttribute.CuryConvCury<SOLine.curyUnitPrice>(
                        //            sograph.Transactions.Cache,
                        //            line);
                        //    }
                        //}

                        line = PXCache<SOLine>.CreateCopy(sograph.Transactions.Update(line));
                        EAWorkOrderLine upd = PXCache<EAWorkOrderLine>.CreateCopy(l);
                        l.CSOrderNbr = line.OrderNbr;
                        l.CSLineNbr = line.LineNbr;
                        this.WOParts.Update(l);
                    }
                    using (PXTransactionScope scope = new PXTransactionScope())
                    {
                        try
                        {
                            if (sograph.IsDirty) sograph.Save.Press();
                            EAWorkOrder upd = PXCache<EAWorkOrder>.CreateCopy(item);
                            //upd.Quoted = true;
                            result = this.WorkOrders.Update(upd);
                            this.Save.Press();
                        }
                        catch
                        {
                            this.Clear();
                            throw;
                        }
                        scope.Complete();
                    }
                }
                else
                {
                    EAWorkOrder upd = PXCache<EAWorkOrder>.CreateCopy(item);
                    //upd.Quoted = true;
                    result = this.WorkOrders.Update(upd);
                    this.Save.Press();
                }
                yield return result;
            }
            if (list.Count == 1 && adapter.MassProcess == true)
            {
                sograph.Clear();
                sograph.SelectTimeStamp();
                sograph.Document.Current = list[0];
                throw new PXRedirectRequiredException(sograph, PX.Objects.SO.Messages.SOOrder);
            }
        }

        public PXAction<EAWorkOrder> ViewSOOrder;
        [PXUIField(DisplayName = "View Order", Visible = false)]
        [PXLookupButton]
        public virtual IEnumerable viewSOOrder(PXAdapter adapter)
        {
            if (this.SOOrders.Current != null)
            {
                SOOrderEntry graph = PXGraph.CreateInstance<SOOrderEntry>();
                graph.Document.Current = graph.Document.Search<SOOrder.orderNbr>(this.SOOrders.Current.OrderNbr, this.SOOrders.Current.OrderType);
                if (graph.Document.Current != null)
                    throw new PXRedirectRequiredException(graph, true, "View Order") { Mode = PXBaseRedirectException.WindowMode.NewWindow };
            }
            return adapter.Get();
        }

        #region Functions

        public static InventoryItem GetInventoryItemRow(PXGraph graph, int? inventoryID)
        {
            if (!inventoryID.HasValue)
                return (InventoryItem)null;

            return (InventoryItem)PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>.Select(graph, new object[1]
                {
                    (object)inventoryID
                }); 
        }

        private bool ValidateOpenState(EAWorkOrderLine row, PXErrorLevel level)
        {
            bool result = true;
            Type[] requestOnOpen =
                row.InventoryID != null
                    ? new Type[] { typeof(EAWorkOrderLine.uOM), typeof(EAWorkOrderLine.siteID), typeof(EAWorkOrderLine.subItemID) }
                    : row.LineType == POLineType.NonStock
                          ? new Type[] { typeof(EAWorkOrderLine.uOM), typeof(EAWorkOrderLine.siteID), }
                          : new Type[] { typeof(EAWorkOrderLine.uOM) };


            foreach (Type type in requestOnOpen)
            {
                object value = this.WOParts.Cache.GetValue(row, type.Name);
                if (value == null)
                {
                    this.WOParts.Cache.RaiseExceptionHandling(type.Name, row, null,
                        new PXSetPropertyException("Should be defined before order creation.", level));
                    result = false;
                }
                else
                    this.WOParts.Cache.RaiseExceptionHandling(type.Name, row, value, null);
            }
            return result;
        }

        private long? CopyCurrenfyInfo(PXGraph graph, long? SourceCuryInfoID)
        {
            CurrencyInfo curryInfo = currencyinfo.Select(SourceCuryInfoID);
            curryInfo.CuryInfoID = null;
            graph.Caches[typeof(CurrencyInfo)].Clear();
            curryInfo = (CurrencyInfo)graph.Caches[typeof(CurrencyInfo)].Insert(curryInfo);
            return curryInfo.CuryInfoID;
        }

        #endregion

    }
}