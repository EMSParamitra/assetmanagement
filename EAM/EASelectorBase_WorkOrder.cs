using System;

namespace EAM
{
    public static class EASelectorBase_WorkOrder
    {
        public static Type[] selectorColumns = new Type[6]
    {
      typeof(EAWorkOrder.eAWorkOrderID),
      typeof(EAWorkOrder.description),
      typeof(EAWorkOrder.status),
      typeof(EAWorkOrder.equipmentID),
      typeof(EAWorkOrder.wOClassID),
      typeof(EAWorkOrder.wOType)
    };
    }
}