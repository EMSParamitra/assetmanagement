using System;
using PX.Data;
using PX.Objects;

namespace EAM
{
  public class EAEquipmentInq : PXGraph<EAEquipmentInq>
  {

    public PXFilter<EquipmentFilter> Filter;
    public PXCancel<EquipmentFilter> Cancel;
    
    [Serializable]
    public class EquipmentFilter : IBqlTable
    {
      #region EAEquipmentCD
      public abstract class eAEquipmentCD : PX.Data.IBqlField { }
      protected String _EAEquipmentCD;
      [PXString(15, IsUnicode = true)]
      [PXUIField(DisplayName = "Equipment Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
      [EASelectorEquipmentNbr]
      public virtual string EAEquipmentCD 
      { 
        get { return this._EAEquipmentCD; } 
        set { this._EAEquipmentCD= value; } 
      } 
      #endregion
    
      #region LicPlateNbr
      public abstract class licPlateNbr : PX.Data.IBqlField { }
      protected string _LicPlateNbr;
      [PXString(10, IsUnicode = true)]
      [PXUIField(DisplayName = "Licensed Plate Nbr.", Visibility = PXUIVisibility.Visible)]
      public virtual string LicPlateNbr
      {
        get { return this._LicPlateNbr; }
        set { this._LicPlateNbr = value; }
      }  
      #endregion
    }


    [PXFilterable]
    public PXSelect<EAEquipment,
             Where2<
              Where<Current<EquipmentFilter.eAEquipmentCD>, IsNull,
                 Or<EAEquipment.eAEquipmentCD, Equal<Current<EquipmentFilter.eAEquipmentCD>>>>,
                And<
              Where<Current<EquipmentFilter.licPlateNbr>, IsNull,
                 Or<EAEquipment.licPlateNbr, Equal<Current<EquipmentFilter.licPlateNbr>>>>>>> CurrentEquipment;   

  }
}