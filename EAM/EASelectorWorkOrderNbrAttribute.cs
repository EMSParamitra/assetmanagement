using PX.Data;

namespace EAM
{
    public class EASelectorWorkOrderNbrAttribute : PXSelectorAttribute
    {
        public EASelectorWorkOrderNbrAttribute()
            : base(typeof(Search3<EAWorkOrder.eAWorkOrderID, OrderBy<Asc<EAWorkOrder.eAWorkOrderID>>>), EASelectorBase_WorkOrder.selectorColumns)
        {
        }
    }
}