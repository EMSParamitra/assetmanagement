using System;
using PX.Objects.CR;
using PX.Data;
using PX.Objects.AR;

namespace EAM
{
  public class EASelectorCustomerAttribute : PXDimensionSelectorAttribute
  {
    public EASelectorCustomerAttribute()
      : base("BIZACCT", typeof (Search2<Customer.bAccountID, 
          LeftJoin<PX.Objects.CR.Contact, 
            On<PX.Objects.CR.Contact.bAccountID, Equal<Customer.bAccountID>, 
            And<PX.Objects.CR.Contact.contactID, Equal<PX.Objects.AR.Customer.defContactID>>>, 
          LeftJoin<Address, 
            On<Address.bAccountID, Equal<Customer.bAccountID>, 
            And<Address.addressID, Equal<PX.Objects.AR.Customer.defAddressID>>>>>, 
          Where<PX.Objects.AR.Customer.type, Equal<BAccountType.customerType>, 
            Or<PX.Objects.AR.Customer.type, Equal<BAccountType.combinedType>>>>), 
              typeof (PX.Objects.AR.Customer.acctCD), 
              typeof (PX.Objects.AR.Customer.acctCD), 
              typeof (PX.Objects.AR.Customer.status), 
              typeof (PX.Objects.AR.Customer.acctName), 
              typeof (PX.Objects.CR.BAccount.classID), 
              typeof (PX.Objects.CR.Contact.phone1), 
              typeof (Address.city), 
              typeof (Address.countryID))
    {
      this.DescriptionField = typeof (PX.Objects.AR.Customer.acctName);
      this.DirtyRead = true;
    }
  }
}