using System;

namespace EAM
{
  public static class EASelectorBase_Equipment
  {
    public static Type[] selectorColumns = new Type[8]
    {
      typeof(EAEquipment.eAEquipmentCD),
      typeof(EAEquipment.description),
      typeof(EAEquipment.status),
      typeof(EAEquipment.licPlateNbr),
      typeof(EAEquipment.vinNbr),
      typeof(EAEquipment.engineNo),
      typeof(EAEquipment.ownerType),
      typeof(EAEquipment.ownerID)
    };
  }
}