using PX.Data;

namespace EAM
{
  public abstract class ListField_Equipment_Status : IBqlField, IBqlOperand
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.Equipment_Status().ID_LIST, new ID.Equipment_Status().TX_LIST)
      {
      }
    }

    public class Active : Constant<string>
    {
      public Active()
        : base("A")
      {
      }
    }

    public class Grounded : Constant<string>
    {
      public Grounded()
        : base("G")
      {
      }
    }

    //public class Dispose : Constant<string>
    //{
    //  public Dispose()
    //    : base("D")
    //  {
    //  }
    //}
  }
}