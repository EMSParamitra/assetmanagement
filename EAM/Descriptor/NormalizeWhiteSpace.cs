using PX.Data;
using System;
using System.Text.RegularExpressions;

namespace EAM
{
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Parameter)]
  public class NormalizeWhiteSpace : PXEventSubscriberAttribute, IPXFieldUpdatingSubscriber
  {
    public virtual void FieldUpdating(PXCache sender, PXFieldUpdatingEventArgs e)
    {
      if (e.NewValue == null || !(e.NewValue is string) || e.Cancel)
        return;
      string newValue = (string) e.NewValue;
      int length = newValue.Length;
      e.NewValue = (object) Regex.Replace(newValue.Trim(), "\\s+", " ").PadRight(length, ' ');
    }
  }
}