using PX.Data;
using PX.Objects.CR;

namespace EAM
{
  public class EASelectorBusinessAccount_VEAttribute : EASelectorBusinessAccount_BaseAttribute
  {
    public EASelectorBusinessAccount_VEAttribute()
      : base("VENDOR", typeof (Where<PX.Objects.CR.BAccount.type, Equal<BAccountType.vendorType>>))
    {
    }
  }
}