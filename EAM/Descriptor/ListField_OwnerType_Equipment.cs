using PX.Data;

namespace EAM
{
  public abstract class ListField_OwnerType_Equipment : IBqlField, IBqlOperand
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.OwnerType_Equipment().ID_LIST, new ID.OwnerType_Equipment().TX_LIST)
      {
      }
    }

    public class OwnCompany : Constant<string>
    {
      public OwnCompany()
        : base("OW")
      {
      }
    }

    public class Customer : Constant<string>
    {
      public Customer()
        : base("TP")
      {
      }
    }
  }
}