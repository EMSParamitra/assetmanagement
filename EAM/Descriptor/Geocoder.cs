using System;
using System.Collections.Generic;
using System.Xml;

namespace EAM
{
  public static class Geocoder
  {
    public static string ReverseGeocode(LatLng location, string apiKey)
    {
      string xml;
      try
      {
        xml = HttpWebService.MakeRequest(string.Format("{0}Locations/{1},{2}?o=xml&key={3}", (object) "https://dev.virtualearth.net/REST/v1/", (object) location.Latitude, (object) location.Longitude, (object) apiKey));
      }
      catch
      {
        throw new Exception("Failed to connect with Google API. Please check your connection.");
      }
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nameSpace = new XmlNamespaceManager(xmlDocument.NameTable);
      SharedFunctions.GenerateXmlNameSpace(ref nameSpace);
      string str1 = string.Empty;
      string[] strArray = new string[2]
      {
        "Address",
        "PopulatedPlace"
      };
      foreach (string str2 in strArray)
      {
        XmlNode xmlNode = xmlDocument.SelectSingleNode(string.Format("//{0}Location[{0}EntityType = '{1}']//{0}FormattedAddress", (object) "bingSchema:", (object) str2), nameSpace);
        if (xmlNode != null)
        {
          str1 = xmlNode.InnerText;
          break;
        }
      }
      return str1;
    }

    public static GLocation[] Geocode(string address, string apiKey)
    {
      string str;
      try
      {
        str = HttpWebService.MakeRequest(string.Format("{0}Locations/{1}?o=xml&key={2}", (object) "https://dev.virtualearth.net/REST/v1/", (object) address, (object) apiKey));
      }
      catch
      {
        throw new Exception("Failed to connect with Google API. Please check your connection.");
      }
      XmlDocument xmlDocument = new XmlDocument();
      string xml = str;
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nameSpace = new XmlNamespaceManager(xmlDocument.NameTable);
      SharedFunctions.GenerateXmlNameSpace(ref nameSpace);
      string xpath1 = string.Format("//{0}Location", (object) "bingSchema:");
      XmlNamespaceManager nsmgr1 = nameSpace;
      XmlNodeList xmlNodeList = xmlDocument.SelectNodes(xpath1, nsmgr1);
      List<GLocation> glocationList = new List<GLocation>();
      foreach (XmlElement xmlElement in xmlNodeList)
      {
        string xpath2 = string.Format(".//{0}FormattedAddress", (object) "bingSchema:");
        XmlNamespaceManager nsmgr2 = nameSpace;
        string innerText = xmlElement.SelectSingleNode(xpath2, nsmgr2).InnerText;
        string xpath3 = string.Format(".//{0}Point", (object) "bingSchema:");
        XmlNamespaceManager nsmgr3 = nameSpace;
        GLocation glocation = new GLocation(new LatLng((XmlElement) xmlElement.SelectSingleNode(xpath3, nsmgr3), nameSpace), innerText);
        glocationList.Add(glocation);
      }
      return glocationList.ToArray();
    }

    public static string GetStatus(string address, string apiKey)
    {
      string str;
      try
      {
        str = HttpWebService.MakeRequest(string.Format("{0}Locations/{1}?o=xml&key={2}", (object) "https://dev.virtualearth.net/REST/v1/", (object) address, (object) apiKey));
      }
      catch
      {
        throw new Exception("Failed to connect with Google API. Please check your connection.");
      }
      XmlDocument xmlDocument = new XmlDocument();
      string xml = str;
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nameSpace = new XmlNamespaceManager(xmlDocument.NameTable);
      SharedFunctions.GenerateXmlNameSpace(ref nameSpace);
      string xpath = string.Format("//{0}StatusDescription", (object) "bingSchema:");
      XmlNamespaceManager nsmgr = nameSpace;
      return xmlDocument.SelectSingleNode(xpath, nsmgr).InnerText;
    }
  }
}