using System.IO;
using System.Net;

namespace EAM
{
  internal static class HttpWebService
  {
    internal static string MakeRequest(string url)
    {
      using (WebResponse response = WebRequest.Create(url).GetResponse())
      {
        using (Stream responseStream = response.GetResponseStream())
        {
          using (StreamReader streamReader = new StreamReader(responseStream))
            return streamReader.ReadToEnd();
        }
      }
    }
  }
}