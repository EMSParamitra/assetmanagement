using PX.Data;

namespace EAM
{
  public class EASelectorEquipmentTypeAttribute : PXSelectorAttribute
  {
    public EASelectorEquipmentTypeAttribute()
      : base(typeof (Search<EAEquipmentType.eQTypeID>))
    {
      this.SubstituteKey = typeof (EAEquipmentType.eQTypeID);
      this.DescriptionField = typeof (EAEquipmentType.description);
    }
  }
}