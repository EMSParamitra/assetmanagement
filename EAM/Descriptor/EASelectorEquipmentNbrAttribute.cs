using PX.Data;

namespace EAM
{
  public class EASelectorEquipmentNbrAttribute : PXSelectorAttribute
  {
    public EASelectorEquipmentNbrAttribute()
      : base(typeof (Search3<EAEquipment.eAEquipmentCD, OrderBy<Asc<EAEquipment.eAEquipmentCD>>>), EASelectorBase_Equipment.selectorColumns)
    {
    }
  }
}