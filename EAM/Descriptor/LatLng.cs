using System;
using System.Globalization;
using System.Xml;

namespace EAM
{
  public class LatLng
  {
    private double latitude;
    private double longitude;

    public LatLng(double latitude, double longitude)
    {
      this.latitude = latitude;
      this.longitude = longitude;
    }

    internal LatLng(XmlElement locationElement, XmlNamespaceManager nameSpace)
    {
      this.latitude = double.Parse(locationElement.SelectSingleNode(string.Format(".//{0}Latitude", (object) "bingSchema:"), nameSpace).InnerText, (IFormatProvider) CultureInfo.InvariantCulture);
      this.longitude = double.Parse(locationElement.SelectSingleNode(string.Format(".//{0}Longitude", (object) "bingSchema:"), nameSpace).InnerText, (IFormatProvider) CultureInfo.InvariantCulture);
    }

    public double Latitude
    {
      get
      {
        return this.latitude;
      }
    }

    public double Longitude
    {
      get
      {
        return this.longitude;
      }
    }

    public override string ToString()
    {
      return this.latitude.ToString() + ", " + this.longitude.ToString();
    }
  }
}