namespace EAM
{
  public class GLocation
  {
    private LatLng latLng;
    private string locationName;

    public GLocation(string locationName)
    {
      this.locationName = locationName;
    }

    public GLocation(LatLng latLng)
    {
      this.latLng = latLng;
    }

    internal GLocation(LatLng latLng, string locationName)
    {
      this.latLng = latLng;
      this.locationName = locationName;
    }

    public LatLng LatLng
    {
      get
      {
        return this.latLng;
      }
    }

    public string LocationName
    {
      get
      {
        return this.locationName;
      }
    }

    public override string ToString()
    {
      if (this.locationName != null)
        return this.locationName;
      return this.latLng.ToString();
    }
  }
}