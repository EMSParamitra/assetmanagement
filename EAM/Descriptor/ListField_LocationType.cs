using PX.Data;

namespace EAM
{
  public abstract class ListField_LocationType : IBqlField, IBqlOperand
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.LocationType().ID_LIST, new ID.LocationType().TX_LIST)
      {
      }
    }

    public class Company : Constant<string>
    {
      public Company()
        : base("CO")
      {
      }
    }

    public class Customer : Constant<string>
    {
      public Customer()
        : base("CU")
      {
      }
    }
  }
}