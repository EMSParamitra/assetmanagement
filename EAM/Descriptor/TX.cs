using System;
using PX.Common;

namespace EAM
{
  public class TX
  {
    [PXLocalizable]
    public static class WarrantyType
    {
      public const string DAY = "Day(s)";
      public const string MONTH = "Month(s)";
      public const string YEAR = "Year(s)";
    }
  
    [PXLocalizable]
    public static class Equipment_Status
    {
      public const string ACTIVE = "Active";
      public const string GROUNDED = "Grounded";
    }

    [PXLocalizable]
    public static class Warranty_Status
    {
        public const string ACTIVE = "Active";
        public const string VOID = "Void";
        public const string EXPIRED = "Expired";
    }
    
    [PXLocalizable]
    public class OwnerType_Equipment
    {
      public const string CUSTOMER = "Customer";
      public const string OWN_COMPANY = "Company";
    }
      
    [PXLocalizable]
    public static class LocationType
    {
      public const string COMPANY = "Company";
      public const string CUSTOMER = "Customer";
    }
  }
}