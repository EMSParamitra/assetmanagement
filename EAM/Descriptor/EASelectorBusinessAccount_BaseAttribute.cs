using System;
using PX.Data;
using PX.Data.EP;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.CR.MassProcess;

namespace EAM
{
  public class EASelectorBusinessAccount_BaseAttribute : PXDimensionSelectorAttribute
  {
    public EASelectorBusinessAccount_BaseAttribute(string dimensionName, System.Type whereType)
      : base(dimensionName, 
          BqlCommand.Compose(
            typeof (Search2<,,>), 
            typeof (BAccountSelectorBase.bAccountID), 
            typeof (LeftJoin<,,>), 
            typeof (PX.Objects.CR.Contact), 
            typeof (On<PX.Objects.CR.Contact.bAccountID, 
              Equal<BAccountSelectorBase.bAccountID>, 
              And<PX.Objects.CR.Contact.contactID, 
                Equal<PX.Objects.CR.BAccount.defContactID>>>), 
            typeof (LeftJoin<,,>), 
            typeof (Address), 
            typeof (On<Address.bAccountID, 
              Equal<BAccountSelectorBase.bAccountID>, 
              And<Address.addressID, 
                Equal<PX.Objects.CR.BAccount.defAddressID>>>), 
                  typeof (LeftJoin<,>), 
                  typeof (PX.Objects.AR.Customer), 
                  typeof (On<PX.Objects.AR.Customer.bAccountID, Equal<BAccountSelectorBase.bAccountID>>), 
                  typeof (Where<>), whereType), 
                  typeof (PX.Objects.CR.BAccount.acctCD), 
                  typeof (PX.Objects.CR.BAccount.acctCD), 
                  typeof (PX.Objects.CR.BAccount.acctName), 
                  typeof (PX.Objects.CR.BAccount.type), 
                  typeof (PX.Objects.AR.Customer.customerClassID), 
                  typeof (PX.Objects.CR.BAccount.status), 
                  typeof (PX.Objects.CR.Contact.phone1), 
                  typeof (Address.addressLine1), typeof (Address.addressLine2), 
                  typeof (Address.postalCode), 
                  typeof (Address.city), 
                  typeof (Address.countryID))
    {
      this.DescriptionField = typeof (PX.Objects.CR.BAccount.acctName);
    }
  }
}