using System;
using PX.Data;

namespace EAM
{
  public class EASelectorWOClassAttribute : PXSelectorAttribute
  {
    public EASelectorWOClassAttribute()
      : base(typeof (Search<EAWorkOrderClass.wOClassID>))
    {
      this.SubstituteKey = typeof (EAWorkOrderClass.wOClassCD);
      this.DescriptionField = typeof (EAWorkOrderClass.descr);
    }
  }
}