using PX.Data;
using PX.Data.EP;
using PX.Objects.CR.MassProcess;
using System;

namespace EAM
{
  //[PXPrimaryGraph(typeof (AccountMaintBridge))]
  [Serializable]
  public class BAccountSelectorBase : PX.Objects.CR.BAccount
  {
    [PXDBString(60, IsUnicode = true)]
    [PXDefault]
    [PXFieldDescription]
    [PXMassMergableField]
    [PXUIField(DisplayName = "Account Name", Visibility = PXUIVisibility.SelectorVisible)]
    public override string AcctName { get; set; }

    public abstract class bAccountID : IBqlField, IBqlOperand
    {
    }
  }
}