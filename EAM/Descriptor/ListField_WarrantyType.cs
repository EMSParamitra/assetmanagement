using PX.Data;

namespace EAM
{
  public abstract class ListField_WarrantyType : IBqlField, IBqlOperand
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.WarrantyType().ID_LIST, new ID.WarrantyType().TX_LIST)
      {
      }
    }

    public class Day : Constant<string>
    {
      public Day()
        : base("D")
      {
      }
    }

    public class Month : Constant<string>
    {
      public Month()
        : base("M")
      {
      }
    }

    public class Year : Constant<string>
    {
      public Year()
        : base("Y")
      {
      }
    }
  }
}