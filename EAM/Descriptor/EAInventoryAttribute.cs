using System;
using PX.Data;
using PX.Objects.GL;
using PX.Objects.IN;

namespace EAM
{
  [PXDBInt]
  [PXUIField(DisplayName = "Service ID", Visibility = PXUIVisibility.Visible)]
  [PXRestrictor(typeof (Where<InventoryItem.itemStatus, NotEqual<InventoryItemStatus.inactive>, And<InventoryItem.itemStatus, NotEqual<InventoryItemStatus.markedForDeletion>>>), "The inventory item is {0}.", new System.Type[] {typeof (InventoryItem.itemStatus)})]
  public class EAInventoryAttribute : AcctSubAttribute
  {
    public const string DimensionName = "INVENTORY";

    public EAInventoryAttribute(System.Type searchType, System.Type substituteKey, System.Type descriptionField, System.Type[] listField)
    {
      this._Attributes.Add((PXEventSubscriberAttribute) new PXDimensionSelectorAttribute("INVENTORY", searchType, substituteKey, listField)
      {
        CacheGlobal = true,
        DescriptionField = descriptionField
      });
      this._SelAttrIndex = this._Attributes.Count - 1;
    }

    public EAInventoryAttribute(System.Type searchType, System.Type substituteKey, System.Type descriptionField)
    {
      this._Attributes.Add((PXEventSubscriberAttribute) new PXDimensionSelectorAttribute("INVENTORY", searchType, substituteKey)
      {
        CacheGlobal = true,
        DescriptionField = descriptionField
      });
      this._SelAttrIndex = this._Attributes.Count - 1;
    }

    public EAInventoryAttribute()
      : this(typeof (Search<InventoryItem.inventoryID, Where<Match<Current<AccessInfo.userName>>>>), typeof (InventoryItem.inventoryCD), typeof (InventoryItem.descr))
    {
    }

    public class dimensionName : Constant<string>
    {
      public dimensionName()
        : base("INVENTORY")
      {
      }
    }
  }
}