using PX.Data;
using PX.Objects.IN;

namespace EAM
{
  [PXDBInt]
  [PXUIField(DisplayName = "Model", Visibility = PXUIVisibility.Visible)]
  public class EAEquipmentModelItemAttribute : EAInventoryAttribute
  {
    private static System.Type[] defaultHeaders = new System.Type[7]
    {
      typeof (InventoryItem.inventoryCD),
      typeof (InventoryItem.descr),
      typeof (InventoryItem.itemClassID),
      typeof (InventoryItem.itemType),
      typeof (InventoryItem.baseUnit),
      typeof (InventoryItem.salesUnit),
      typeof (InventoryItem.basePrice)
    };

    public EAEquipmentModelItemAttribute(System.Type[] headers = null)
      : this(typeof (Where<True, Equal<True>>), headers)
    {
    }

    public EAEquipmentModelItemAttribute(System.Type whereType, System.Type[] headers = null)
      : base(BqlCommand.Compose(typeof (Search<,>), typeof (InventoryItem.inventoryID)), typeof (InventoryItem.inventoryCD), typeof (InventoryItem.descr), headers ?? EAEquipmentModelItemAttribute.defaultHeaders)
    {
    }
  }
}