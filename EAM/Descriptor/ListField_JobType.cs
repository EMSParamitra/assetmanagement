using PX.Data;

namespace EAM
{
  public abstract class ListField_JobType : IBqlField, IBqlOperand
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.JobType().ID_LIST, new ID.JobType().TX_LIST)
      {
      }
    }

    public class Assembly : Constant<string>
    {
      public Assembly()
        : base("ASY")
      {
      }
    }

    public class PDI : Constant<string>
    {
      public PDI()
        : base("PDI")
      {
      }
    }

    public class Fullfilment : Constant<string>
    {
      public Fullfilment()
        : base("FFM")
      {
      }
    }
    
    public class PreCheck : Constant<string>
    {
      public PreCheck()
        : base("PCD")
      {
      }
    }
    
    public class WarrantyClaim : Constant<string>
    {
      public WarrantyClaim()
        : base("WCL")
      {
      }
    }
    
    public class VoucherService : Constant<string>
    {
      public VoucherService()
        : base("VCS")
      {
      }
    }
    
    public class Cash : Constant<string>
    {
      public Cash()
        : base("CSH")
      {
      }
    }
    
    public class Other : Constant<string>
    {
      public Other()
        : base("OTH")
      {
      }
    }
  }
}