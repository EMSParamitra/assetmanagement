using PX.Data;

namespace EAM
{
  public abstract class ListField_WorkOrder_Status : IBqlField, IBqlOperand
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.WorkOrder_Status().ID_LIST, new ID.WorkOrder_Status().TX_LIST)
      {
      }
    }

    public class OnHold : Constant<string>
    {
        public OnHold()
            : base("H")
        {
        }
    }

    public class Open : Constant<string>
    {
      public Open()
        : base("O")
      {
      }
    }

    public class InProgress : Constant<string>
    {
      public InProgress()
        : base("I")
      {
      }
    }

    public class Close : Constant<string>
    {
      public Close()
        : base("C")
      {
      }
    }
  }
}