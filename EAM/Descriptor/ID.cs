using System;

namespace EAM
{
  public static class ID
  {
    public class WarrantyType
    {
      public readonly string[] ID_LIST = new string[3]
      {
        "D",
        "M",
        "Y"
      };
      public readonly string[] TX_LIST = new string[3]
      {
        "Day(s)",
        "Month(s)",
        "Year(s)"
      };
      public const string DAY = "D";
      public const string MONTH = "M";
      public const string YEAR = "Y";
    }
    
    public class Equipment_Status
    {
      public readonly string[] ID_LIST = new string[2]
      {
        "A",
        "G"
      };
      public readonly string[] TX_LIST = new string[2]
      {
        "Active",
        "Grounded"
      };
      public const string ACTIVE = "A";
      public const string GROUNDED = "G";
    }

    public class Warranty_Status
    {
        public readonly string[] ID_LIST = new string[3]
      {
        "A",
        "V",
        "E"
      };
        public readonly string[] TX_LIST = new string[3]
      {
        "Active",
        "Void",
        "Expired"
      };
        public const string ACTIVE = "A";
        public const string VOID = "V";
        public const string EXPIRED = "E";
    }
    
    public class OwnerType
    {
      public readonly string[] ID_LIST = new string[2]
      {
        "B",
        "E"
      };
      public readonly string[] TX_LIST = new string[2]
      {
        "Business",
        "Staff Member"
      };
      public const string BUSINESS = "B";
      public const string EMPLOYEE = "E";
    }

    public class OwnerType_Equipment
    {
      public readonly string[] ID_LIST = new string[2]
      {
        "TP",
        "OW"
      };
      public readonly string[] TX_LIST = new string[2]
      {
        "Customer",
        "Company"
      };
      public const string CUSTOMER = "TP";
      public const string OWN_COMPANY = "OW";
    }
    
    public class LocationType
    {
      public readonly string[] ID_LIST = new string[2]
      {
        "CO",
        "CU"
      };
      public readonly string[] TX_LIST = new string[2]
      {
        "Company",
        "Customer"
      };
      public const string COMPANY = "CO";
      public const string CUSTOMER = "CU";
    }

    public class JobType
    {
        public readonly string[] ID_LIST = new string[8]
      {
        "ASY",
        "PDI",
        "FFM",
        "PCD",
        "WCL",
        "VCS",
        "CSH",
        "OTH"
      };
        public readonly string[] TX_LIST = new string[8]
      {
        "Assembly",
        "PDI",
        "Fullfilment",
        "Pre-Check",
        "Warranty Claim",
        "Voucher Service",
        "Cash",
        "Other"
      };
        public const string ASSEMBLY = "ASY";
        public const string PDI = "PDI";
        public const string FULLFILMENT = "FFM";
        public const string PRECHECK = "PCD";
        public const string WARRANTYCLAIM = "WCL";
        public const string VOUCHERSERVICE = "VCS";
        public const string CASH = "CSH";
        public const string OTHER = "OTH";
    }

    public class WorkOrder_Status
    {
        public readonly string[] ID_LIST = new string[4]
      {
        "H",
        "O",
        "I",
        "C"
      };
        public readonly string[] TX_LIST = new string[4]
      {
        "On Hold",
        "Open",
        "In Progress",
        "Close"
      };
        public const string ONHOLD = "H";
        public const string OPEN = "O";
        public const string INPROGRESS = "I";
        public const string CLOSE = "C";
    }
  }
}