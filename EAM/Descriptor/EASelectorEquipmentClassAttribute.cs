using PX.Data;

namespace EAM
{
  public class EASelectorEquipmentClassAttribute : PXSelectorAttribute
  {
    public EASelectorEquipmentClassAttribute()
      : base(typeof (Search<EAEquipmentClass.eQClassID>))
    {
      this.SubstituteKey = typeof (EAEquipmentClass.eQClassCD);
      this.DescriptionField = typeof (EAEquipmentClass.descr);
    }
  }
}