using PX.Data;
using PX.Data.Update;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.PM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace EAM
{
  public static class SharedFunctions
  {
    
    public static XmlNamespaceManager GenerateXmlNameSpace(ref XmlNamespaceManager nameSpace)
    {
      nameSpace.AddNamespace(string.Format("{0}", (object) "bingSchema"), string.Format("{0}", (object) "http://schemas.microsoft.com/search/local/ws/rest/v1"));
      return nameSpace;
    }
    
  }
}