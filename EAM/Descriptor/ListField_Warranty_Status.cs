using System;
using PX.Data;

namespace EAM
{
  public class ListField_Warranty_Status
  {
    public class ListAtrribute : PXStringListAttribute
    {
      public ListAtrribute()
        : base(new ID.Warranty_Status().ID_LIST, new ID.Warranty_Status().TX_LIST)
      {
      }
    }

    public class Active : Constant<string>
    {
      public Active()
        : base("A")
      {
      }
    }

    public class Void : Constant<string>
    {
      public Void()
        : base("V")
      {
      }
    }

    public class Expired : Constant<string>
    {
        public Expired()
            : base("E")
        {
        }
    }
  }
}