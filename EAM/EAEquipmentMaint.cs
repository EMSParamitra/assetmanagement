using System;
using System.Linq;
using PX.Data;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Collections;
using PX.Common;
using PX.Objects.GL;
using PX.Objects.CR;
using PX.Objects.PO;
using PX.Objects.SO;
using PX.Objects.AR;

namespace EAM
{  
  public class EAEquipmentMaint : PXGraph<EAEquipmentMaint, EAEquipment>
  {

    public PXSelect<EAEquipment> Equipments;
    public PXSelect<EAEquipment, Where<EAEquipment.eAEquipmentCD, Equal<Current<EAEquipment.eAEquipmentCD>>>> CurrentEquipment;
  
    public CRAttributeList<EAEquipment> Answers;
    public PXSelect<EAEquipmentClass, Where<EAEquipmentClass.eQClassID, Equal<Optional<EAEquipment.eQClassID>>>> equipclass;
    public PXSelect<POOrder, Where<POOrder.orderNbr, Equal<Optional<EAEquipment.pOOrderNbr>>,
        And<POOrder.orderType, Equal<Optional<EAEquipment.pOOrderType>>>>> PurchaseOrder;
    public PXSelect<SOOrder, Where<SOOrder.orderNbr, Equal<Optional<EAEquipment.salesOrderNbr>>,
        And<SOOrder.orderType, Equal<Optional<EAEquipment.salesOrderType>>>>> SlsOrder;
    public PXSelectJoin<POOrderReceipt, 
                  InnerJoin<POOrder, 
                      On<POOrder.orderNbr, Equal<POOrderReceipt.pONbr>, 
                        And<POOrder.orderType, Equal<POOrderReceipt.pOType>>>>, 
                  Where<POOrder.orderNbr, Equal<Optional<EAEquipment.pOOrderNbr>>>> PoReceipt;
    public PXSelect<EACustomerHistory, Where<EACustomerHistory.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>,
        And<EACustomerHistory.revisionID, Equal<Current<EAEquipment.ownerRevID>>>>> Owners;
    public PXSelect<EACustomerHistory, Where<EACustomerHistory.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>,
        And<EACustomerHistory.ownerType, Equal<EAEquipment.ownerType.Customer>, And<EACustomerHistory.ownerID, IsNotNull>>>,
        OrderBy<Desc<EACustomerHistory.revisionID>>> OwnerHistory;
    public PXSelectJoin<Address,
                  InnerJoin<Location,
                  On<Address.addressID, Equal<Location.defAddressID>>>,
                  Where<Location.locationID, Equal<Current<EACustomerHistory.customerLocationID>>,
                  And<Location.bAccountID, Equal<Current<EACustomerHistory.ownerID>>>>> OwnerAddress;
    public PXSelectJoin<Contact,
        InnerJoin<Customer,
        On<Contact.bAccountID, Equal<Customer.bAccountID>>>,
        Where<Contact.bAccountID, Equal<Current<EACustomerHistory.ownerID>>,
        And<Customer.defLocationID, Equal<Current<EACustomerHistory.customerLocationID>>>>> customerContact;

    public PXSelect<EAWorkOrder, Where<EAWorkOrder.equipmentID, Equal<Current<EAEquipment.eAEquipmentID>>>> ServiceHist;
  
    public PXSetup<EAEquipmentClass> EASetup;
  
    public EAEquipmentMaint()
    {
      EAEquipmentClass setup = EASetup.Current;
    }

    protected void EAEquipment_EQClassID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {
        if (e.Row == null) return;
      
        EAEquipment row = (EAEquipment)e.Row;
        if (row.EQClassID != null)
        {
            EAEquipmentClass ec = equipclass.Select();
            sender.SetValue<EAEquipment.eQTypeID>(e.Row, ec.EQTypeID);
        }
    }

    protected void EAEquipment_POOrderNbr_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {
      
      EAEquipment row = (EAEquipment)e.Row;
  
      POOrder po = PurchaseOrder.Select();
      POOrderReceipt pr = PoReceipt.Select();
  
      sender.SetValueExt<EAEquipment.pOOrderDate>(e.Row, po.OrderDate);
      sender.SetValueExt<EAEquipment.pOReceiptNbr>(e.Row, pr.ReceiptNbr);
      
    }

    protected void EAEquipment_SalesOrderNbr_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {

        EAEquipment row = (EAEquipment)e.Row;

        SOOrder so = SlsOrder.Select();

        sender.SetValueExt<EAEquipment.salesDate>(e.Row, so.ShipDate);
    }
    
    protected void EAEquipment_SalesDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {
        EAEquipment row = (EAEquipment)e.Row;

        sender.SetValueExt<EAEquipment.cpnyWarrantyEndDate>(e.Row, ((DateTime)row.SalesDate).AddYears(3));
    }

    protected void EAEquipment_CpnyWarrantyEndDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {
        EAEquipment row = (EAEquipment)e.Row;

        if (row.CpnyWarrantyEndDate < Accessinfo.BusinessDate)
        {
            sender.SetValueExt<EAEquipment.warrantyStatus>(e.Row, "E");
            PXUIFieldAttribute.SetEnabled<EAEquipment.warrantyStatus>(sender, e.Row, false);
        }
    }

    #region Functions
    private static void CopyOwner(EACustomerHistory from, EACustomerHistory to)
    {
        to.OwnerType = from.OwnerType;
        to.OwnerID = from.OwnerID;
    }
    #endregion

    #region EACustomerHistory Events

    protected virtual void EACustomerHistory_RowUpdating(PXCache sender, PXRowUpdatingEventArgs e)
    {
        EACustomerHistory customerHist = e.NewRow as EACustomerHistory;
        if (customerHist == null) return;

        EACustomerHistory transfer = PXSelect<EACustomerHistory, Where<EACustomerHistory.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>>>
            .SelectSingleBound(this, new object[] { customerHist });

        if (this.IsOwnerChanged(e.Row as EACustomerHistory, customerHist)) //transfer == null &&
        {
            EACustomerHistory newOwner = PXCache<EACustomerHistory>.CreateCopy(customerHist);
            try
            {
                sender.SmartSetStatus(e.Row, PXEntryStatus.Notchanged, PXEntryStatus.Updated);
                newOwner.RevisionID = CurrentEquipment.Current.OwnerRevID;
                newOwner.TransactionDate = Accessinfo.BusinessDate;
                newOwner = (EACustomerHistory)sender.Insert(newOwner);
            }
            finally
            {
                if (!(e.Cancel = (newOwner != null)))
                {
                    sender.SmartSetStatus(e.Row, PXEntryStatus.Updated);
                }
            }
        }
    }

    protected virtual void EACustomerHistory_RowInserting(PXCache sender, PXRowInsertingEventArgs e)
    {
        EACustomerHistory newOwner = (EACustomerHistory)e.Row;
        if (newOwner == null) return;
        foreach (EACustomerHistory row in sender.Cached)
        {
            PXEntryStatus status = sender.GetStatus(row);
            if (status == PXEntryStatus.Inserted || status == PXEntryStatus.Updated)
            {
                if(newOwner.OwnerType != "OW")
                {
                    CopyOwner(newOwner, row);
                    e.Cancel = true;
                    break;
                }
            }
        }

        if (!e.Cancel)
        {
            if (CurrentEquipment.Current == null)
            {
                CurrentEquipment.Current = CurrentEquipment.Select();
            }
            CurrentEquipment.Current.OwnerRevID = ++newOwner.RevisionID;
            CurrentEquipment.Current.OwnerID = newOwner.OwnerID;
            CurrentEquipment.Cache.Update(CurrentEquipment.Current);
        }
    }

    protected virtual void EACustomerHistory_CustomerLocationID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {
        EACustomerHistory row = (EACustomerHistory)e.Row;

        if (row.CustomerLocationID != null)
        {
            Address oa = OwnerAddress.Select();
            Contact cust = customerContact.Select();

            sender.SetValue<EACustomerHistory.addressLine1>(e.Row, oa.AddressLine1);
            sender.SetValue<EACustomerHistory.addressLine2>(e.Row, oa.AddressLine2);
            sender.SetValue<EACustomerHistory.addressLine3>(e.Row, oa.AddressLine3);
            sender.SetValue<EACustomerHistory.city>(e.Row, oa.City);
            sender.SetValue<EACustomerHistory.countryID>(e.Row, oa.CountryID);
            sender.SetValue<EACustomerHistory.state>(e.Row, oa.State);
            sender.SetValue<EACustomerHistory.postalCode>(e.Row, oa.PostalCode);
            sender.SetValue<EACustomerHistory.transactionDate>(e.Row, Accessinfo.BusinessDate);
            sender.SetValue<EACustomerHistory.phone>(e.Row, cust.Phone1);
        }

    }

    //protected virtual void EACustomerHistory_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
    //{
    //    EACustomerHistory row = (EACustomerHistory)e.Row;
    //    if (row.OwnerType == null)
    //    {
    //        PXUIFieldAttribute.SetEnabled<EACustomerHistory.ownerID>(sender, row, false);
    //    }
    //    else
    //    {
    //        this.EnableDisableCustomer(sender, row);
    //    }
    //}

    protected void EACustomerHistory_OwnerType_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
    {
        EACustomerHistory row = (EACustomerHistory)e.Row;

        if (row.OwnerType == "OW")
        {
            sender.SetValue<EACustomerHistory.ownerID>(e.Row, null);
            sender.SetValue<EACustomerHistory.customerLocationID>(e.Row, null);
            sender.SetValue<EACustomerHistory.addressLine1>(e.Row, null);
            sender.SetValue<EACustomerHistory.addressLine2>(e.Row, null);
            sender.SetValue<EACustomerHistory.addressLine3>(e.Row, null);
            sender.SetValue<EACustomerHistory.city>(e.Row, null);
            sender.SetValue<EACustomerHistory.state>(e.Row, null);
            sender.SetValue<EACustomerHistory.countryID>(e.Row, null);
            sender.SetValue<EACustomerHistory.postalCode>(e.Row, null);
            sender.SetValue<EACustomerHistory.phone>(e.Row, null);
            sender.SetValue<EACustomerHistory.eMail>(e.Row, null);
            sender.SetValue<EACustomerHistory.reason>(e.Row, null);
        }
    }

    private void EnableDisableCustomer(PXCache sender, EACustomerHistory customerRow)
    {
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.ownerID>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.customerLocationID>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.addressLine1>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.addressLine2>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.addressLine3>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.state>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.countryID>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.city>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.postalCode>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.phone>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.eMail>(sender, (object)customerRow, customerRow.OwnerType == "TP");
        PXUIFieldAttribute.SetEnabled<EACustomerHistory.reason>(sender, (object)customerRow, customerRow.OwnerType == "TP");
    }

    #endregion
  }

  public static class EAHelper
  {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static bool IsOwnerChanged(this PXGraph graph, EACustomerHistory current, EACustomerHistory prev=null)
      {
          return !graph.Caches<EACustomerHistory>().ObjectsEqual<
          EACustomerHistory.ownerType,
          EACustomerHistory.ownerID>(prev ?? graph.GetPrevCustomer(current), current);
      }

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static EACustomerHistory GetCurrentCustomer(this PXGraph graph, EAEquipment details)
      {
          EACustomerHistory current = PXSelect<EACustomerHistory,
              Where<EACustomerHistory.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>,
                  And<EACustomerHistory.revisionID, Equal<Current<EAEquipment.ownerRevID>>>>>
              .SelectSingleBound(graph, new object[] { details });
          return current;
      }
      
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static EACustomerHistory GetPrevCustomer(this PXGraph graph, EACustomerHistory current)
      {
          EACustomerHistory prev = PXSelect<EACustomerHistory,
              Where<EACustomerHistory.eAEquipmentID, Equal<Current<EACustomerHistory.eAEquipmentID>>,
                  And<EACustomerHistory.revisionID, Less<Current<EACustomerHistory.revisionID>>>>,
                      OrderBy<Desc<EACustomerHistory.revisionID>>>.SelectSingleBound(graph, new object[] { current });
          return prev;
      }

  }

  public class OwnerProcess : PXGraph<OwnerProcess>
  {
      public static void ChangeOwner(PXGraph graph, EAEquipment equipment, EACustomerHistory owner)
      {
          PXCache ownerCache = graph.Caches[typeof(EACustomerHistory)];
          PXCache equipCache = graph.Caches[typeof(EAEquipment)];
          EASetup easetup = (EASetup)graph.Caches[typeof(EASetup)].Current;

          if (!equipCache.IsInsertedUpdatedDeleted)
          {
              equipCache.ClearQueryCache();
              equipCache.Clear();
              graph.Caches<EAEquipment>().Current =
                  equipment = PXSelectReadonly<EAEquipment, Where<EAEquipment.eAEquipmentID, Equal<Current<EAEquipment.eAEquipmentID>>>>.SelectSingleBound(graph, new object[] { equipment });
          }

          EACustomerHistory oldOwner = PXCache<EACustomerHistory>.CreateCopy(owner);

      }
  }
}