using System;
using PX.Data;

namespace EAM
{
  public class EQTypeMaint : PXGraph<EQTypeMaint>
  {

    public PXSave<EAEquipmentType> Save;
    public PXCancel<EAEquipmentType> Cancel;

    #region Selects Declaration

    public PXSelect<EAEquipmentType> EquipmentTypes;
    
    #endregion

    #region Ctor
    
    public EQTypeMaint()
    {
      EquipmentTypes.Cache.AllowInsert = true;
      EquipmentTypes.Cache.AllowUpdate = true;
      EquipmentTypes.Cache.AllowDelete = true;
    }
    
    #endregion
    
    #region Events
    

    protected virtual void EAEquipmentType_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
    {
      EAEquipmentType row = (EAEquipmentType)e.Row;
      if (row == null) return;
      PXUIFieldAttribute.SetEnabled<EAEquipmentType.eQTypeID>(sender, row, sender.GetStatus(row) == PXEntryStatus.Inserted);
    }
    
    #endregion

  }
}