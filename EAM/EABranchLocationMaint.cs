using System;
using PX.Data;
using PX.Objects;
using PX.Objects.GL;
using PX.Objects.CS;
using PX.SM;
using System.Collections;
using System.Net;

namespace EAM
{
  public class EABranchLocationMaint : PXGraph<EABranchLocationMaint, EABranchLocation>
  {

    [PXHidden]
    public PXSetup<EASetup> SetupRecord;
    public PXSelect<EABranchLocation> BranchLocationRecords;
    public PXSelect<PX.Objects.GL.Branch, Where<PX.Objects.GL.Branch.branchID, Equal<Current<EABranchLocation.branchID>>>> Branch;
  
    public PXAction<EABranchLocation> viewMainOnMap;
    [PXButton(CommitChanges = true)]
    [PXUIField(DisplayName = "View On Map", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
    public virtual IEnumerable ViewMainOnMap(PXAdapter adapter)
    {
      GoogleMapRedirector googleMapRedirector = new GoogleMapRedirector();
      Country country = (Country) PXSelect<Country, Where<Country.countryID, Equal<Current<EABranchLocation.countryID>>>>.Select((PXGraph) this);
      State state1 = (State) PXSelect<State, Where<State.stateID, Equal<Current<EABranchLocation.state>>>>.Select((PXGraph) this);
      if (country != null)
      {
        string description = country.Description;
        string state2 = string.Empty;
        if (state1 != null)
          state2 = state1.Name;
        string city = this.BranchLocationRecords.Current.City;
        string postalCode = this.BranchLocationRecords.Current.PostalCode;
        string addressLine1 = this.BranchLocationRecords.Current.AddressLine1;
        string addressLine2 = this.BranchLocationRecords.Current.AddressLine2;
        string addressLine3 = this.BranchLocationRecords.Current.AddressLine3;
        googleMapRedirector.ShowAddress(description, state2, city, postalCode, addressLine1, addressLine2, addressLine3);
      }
      return adapter.Get();
    }


  }
}