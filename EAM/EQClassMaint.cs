using System;
using PX.Data;
using PX.Data.Maintenance;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.IN;

namespace EAM
{
  public class EQClassMaint : PXGraph<EQClassMaint, EAEquipmentClass>
  {

    public PXSelect<EAEquipmentClass> EquipmentClass;
    public PXSelect<EAEquipmentClass, Where<EAEquipmentClass.eQClassCD, Equal<Current<EAEquipmentClass.eQClassCD>>>> CurrentEquipmentClass;
  
    public CSAttributeGroupList<EAEquipmentClass, EAEquipment> Mapping;

  }
}